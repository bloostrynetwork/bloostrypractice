package mc.bloostrynetwork.fr.bloostrypractice.utils.player;

public enum PlayerStatus {

	INSPAWN(true),
	INEDITKIT(true),
	INSPECTATOR(true),
	INFIGHT(false);
	
	private boolean invinsible;
	
	PlayerStatus(boolean invinsible){
		this.invinsible = invinsible;
	}
	
	public boolean isInvinsible() {
		return this.invinsible;
	}
}
