package mc.bloostrynetwork.fr.bloostrypractice.utils.player;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;

import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiEditKit;
import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiRanked;
import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiUnranked;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.ItemHandler;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.scoreboard.PlayerScoreboard;

public class PracticePlayer {


	public final HashMap<Integer, Item> items = new HashMap<>();
	
	private String name;
	private Fight fight;
	
	private PlayerStatus playerStatus;
	private PlayerScoreboard playerScoreboard;
	
	public PracticePlayer(String name) {
		this.name = name;
		this.playerScoreboard = new PlayerScoreboard(this);
		
		if(getPlayerStats() == null){
			PlayerStats.create(getPlayer().getUniqueId().toString());
		}
		
		playerStatus = PlayerStatus.INSPAWN;
	}
	
	public void joinInSpawn() {
		items.put(1, new Item(Material.DIAMOND_SWORD, "�9Ranked", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				new GuiRanked().open(player);
			}
		}));

		items.put(2, new Item(Material.IRON_SWORD, "�9Unranked", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				new GuiUnranked().open(player);
			}
		}));

		items.put(5, new Item(Material.EYE_OF_ENDER, "�9Cr�er une team", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				
			}
		}));

		items.put(9, new Item(Material.BOOK, "�9Modifier un kit", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				new GuiEditKit().open(player);
			}
		}));
		
		clearStuff();
		clearAllEffects();
		Player player = getPlayer();
		player.setGameMode(GameMode.ADVENTURE);
		player.setFoodLevel(20);
		player.setHealth(20.0D);
		player.setFireTicks(0);
		Inventory inventory = player.getInventory();
		for(Integer slot : items.keySet()) {
			Item item = items.get(slot);
			inventory.setItem((slot - 1), item.toItemStack());	
		}
		player.updateInventory();
	}
	
	public void joinInQueue() {
		clearStuff();
		clearAllEffects();
		Player player = getPlayer();
		player.setGameMode(GameMode.ADVENTURE);
		player.setFoodLevel(20);
		player.setHealth(20.0D);
		player.setFireTicks(0);

		items.put(9, new Item(Material.REDSTONE, "�cQuitter la file d'attente", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				PracticeManager.getInstance().leaveQueue(PracticePlayer.this);
				getPlayer().sendMessage(PracticeManager.PREFIX+"�cVous venez de quitter la file d'attente.");
				joinInSpawn();
			}
		}));

		items.put(1, new Item(Material.PAPER, "�eInformation", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				PracticeManager.getInstance().informationQueue(PracticePlayer.this);
			}
		}));
		
		Inventory inventory = player.getInventory();
		for(Integer slot : items.keySet()) {
			Item item = items.get(slot);
			if(item.getName().equalsIgnoreCase("�cQuitter la file d'attente") || item.getName().equalsIgnoreCase("�eInformation")) {
				inventory.setItem((slot - 1), item.toItemStack());	
			}
		}
		player.updateInventory();
	}
	
	public void specFight() {
		clearStuff();
		clearAllEffects();
		Player player = getPlayer();
		player.setGameMode(GameMode.ADVENTURE);
		player.setFoodLevel(20);
		player.setHealth(20.0D);
		player.setFireTicks(0);
		player.setAllowFlight(true);

		items.put(9, new Item(Material.REDSTONE, "�cQuitter la file d'attente", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				PracticeManager.getInstance().leaveQueue(PracticePlayer.this);
				getPlayer().sendMessage(PracticeManager.PREFIX+"�cVous venez de quitter la file d'attente.");
				joinInSpawn();
			}
		}));

		items.put(1, new Item(Material.PAPER, "�eInformation", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				PracticeManager.getInstance().informationQueue(PracticePlayer.this);
			}
		}));
		
		Inventory inventory = player.getInventory();
		for(Integer slot : items.keySet()) {
			Item item = items.get(slot);
			if(item.getName().equalsIgnoreCase("�cQuitter le mode spectateur") || item.getName().equalsIgnoreCase("�eInformation du combat")) {
				inventory.setItem((slot - 1), item.toItemStack());	
			}
		}
		player.updateInventory();
	}
	
	public void clearAllEffects() {
		Player player = getPlayer();
		for(PotionEffect potions : player.getActivePotionEffects()) {
			if(player.hasPotionEffect(potions.getType())) {
				player.removePotionEffect(potions.getType());
			}
		}
	}
	
	public void clearStuff() {
		clearInventory();
		clearEquipments();
		Player player = getPlayer();
		player.setSaturation(15);
	}
	
	public void clearInventory() {
		Player player = getPlayer();
		player.getInventory().clear();
	}
	
	public void clearEquipments() {
		Player player = getPlayer();
		player.getInventory().setArmorContents(null);
	}
	
	public PlayerStats getPlayerStats() {
		return PlayerStats.getPlayerStats(getPlayer().getUniqueId().toString());
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(name);
	}
	
	public void setPlayerStatus(PlayerStatus playerStatus) {
		this.playerStatus = playerStatus;
	}
	
	public PlayerStatus getPlayerStatus() {
		return this.playerStatus;
	}
	
	public boolean isOnline() {
		if(getPlayer() != null)
			return true;
		return false;
	}

	public PlayerScoreboard getPlayerScoreboard() {
		return playerScoreboard;
	}

	public void teleportToSpawn() {
		if(!isOnline())return;
		Player player = getPlayer();
		player.teleport(PracticeManager.getInstance().getSpawnLocation());
	}

	public void setFight(Fight fight) {
		this.fight = fight;
	}

	public Fight getFight() {
		return this.fight;
	}
}
