package mc.bloostrynetwork.fr.bloostrypractice.utils.player.scoreboard;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.ScoreboardCreator;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.FightType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerScoreboard {

	private PracticePlayer practicePlayer;
	private ScoreboardCreator scoreboardCreator;
	private PlayerStatus lastPlayerStatus;

	public PlayerScoreboard(PracticePlayer practicePlayer) {
		this.practicePlayer = practicePlayer;
		this.scoreboardCreator = new ScoreboardCreator(Bukkit.getScoreboardManager().getNewScoreboard(),
				"�8[�cPractice�8]");
	}

	public void refresh() {
		if (!practicePlayer.getPlayerStats().hasScoreboard())
			return;
		PlayerStatus playerStatus = practicePlayer.getPlayerStatus();

		if (playerStatus != lastPlayerStatus) {
			for (int i = 0; i < 15; i++) {
				this.scoreboardCreator.clearText(i);
			}
			lastPlayerStatus = playerStatus;
		}

		if (playerStatus == PlayerStatus.INSPAWN) {
			this.scoreboardCreator.setText(0, "�8�m----------------------------");
			int i = 1;
			for (GameType gameType : GameType.values()) {
				int elo = practicePlayer.getPlayerStats().getElo(gameType);
				this.scoreboardCreator.setText(i, "�c" + gameType.getName() + "�7: �f" + elo);
				i++;
			}
			this.scoreboardCreator.setText(i, "�8�m----------------------------");
			this.scoreboardCreator.setText(i + 1,
					"�cGlobal Elo�7: �f" + practicePlayer.getPlayerStats().getGlobalElo());
			this.scoreboardCreator.setText(i + 2, "�8�m----------------------------");
		} else if (playerStatus == PlayerStatus.INFIGHT) {
			this.scoreboardCreator.setText(0, "�8�m----------------------------");
			this.scoreboardCreator.setText(1,
					"�cAdversaire: �f" + practicePlayer.getFight().getOppenent(this.practicePlayer.getPlayer()));
			this.scoreboardCreator.setText(2,
					"�cDur�e: �f" + (DateUtils.getTimerDate(practicePlayer.getFight().getTimeElapsed())));
			if (practicePlayer.getFight().getFightType() == FightType.RANKED) {
				if(practicePlayer.getFight().isFightTeam()) {
					this.scoreboardCreator.setText(3, "�cElo: �f" + PracticeManager.getInstance()
							.getPracticePlayer(practicePlayer.getFight().getOppenent(this.practicePlayer.getPlayer()))
							.getPlayerStats().getElo(practicePlayer.getFight().getGameType()));	
				}
			}
			this.scoreboardCreator.setText(14, "�8�m----------------------------");
		} else if (playerStatus == PlayerStatus.INEDITKIT) {
		} else if (playerStatus == PlayerStatus.INSPECTATOR) {
		}

		Scoreboard scoreboard = scoreboardCreator.getScoreboard();

		practicePlayer.getPlayer().setScoreboard(scoreboard);

		Team oppenentsPlayerTeam = scoreboard.getTeam("dOPlayers");
		Team alliesPlayerTeam = scoreboard.getTeam("alliesPlayers");
		Team defaultPlayerTeam = scoreboard.getTeam("EDPlayers");

		if (oppenentsPlayerTeam == null) {
			oppenentsPlayerTeam = scoreboard.registerNewTeam("dOPlayers");
			oppenentsPlayerTeam.setPrefix("�c");
		}

		if (alliesPlayerTeam == null) {
			alliesPlayerTeam = scoreboard.registerNewTeam("alliesPlayers");
			alliesPlayerTeam.setPrefix("�a");
		}

		if (defaultPlayerTeam == null) {
			defaultPlayerTeam = scoreboard.registerNewTeam("EDPlayers");
			defaultPlayerTeam.setPrefix("�7");
		}

		Fight fight = practicePlayer.getFight();
		if (fight != null) {
			List<String> blackListPlayer = new ArrayList<>();

			for (String player : fight.getOppenents(practicePlayer.getPlayer())) {
				Player bukkitPlayer = Bukkit.getPlayer(player);
				if (bukkitPlayer != null) {
					addPlayerOppenentsPlayers(bukkitPlayer, oppenentsPlayerTeam, alliesPlayerTeam, defaultPlayerTeam);
					Console.sendMessageDebug("ADDED OPPENENTS -> " + player);
					blackListPlayer.add(player);
				}
			}

			for (String player : fight.getAllies(practicePlayer.getPlayer())) {
				Player bukkitPlayer = Bukkit.getPlayer(player);
				if (bukkitPlayer != null) {
					addPlayerAlliesPlayers(bukkitPlayer, oppenentsPlayerTeam, alliesPlayerTeam, defaultPlayerTeam);
					Console.sendMessageDebug("ADDED ALLIES -> " + player);
					blackListPlayer.add(player);
				}
			}

			for (Player player : Bukkit.getOnlinePlayers()) {
				if (!(blackListPlayer.contains(player.getName()))) {
					addPlayerDefaultPlayers(player, oppenentsPlayerTeam, alliesPlayerTeam, defaultPlayerTeam);
					Console.sendMessageDebug("ADDED DEFAULT -> " + player.getName());
				}
			}

			blackListPlayer.clear();
		} else {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (practicePlayer.getPlayer().getName().equalsIgnoreCase(player.getName()))
					addPlayerAlliesPlayers(player, oppenentsPlayerTeam, alliesPlayerTeam, defaultPlayerTeam);
				else
					addPlayerDefaultPlayers(player, oppenentsPlayerTeam, alliesPlayerTeam, defaultPlayerTeam);
			}
		}
	}

	public void addPlayerOppenentsPlayers(Player player, Team oppenentsPlayerTeam, Team alliesPlayerTeam,
			Team defaultPlayerTeam) {
		alliesPlayerTeam.removePlayer(player);
		defaultPlayerTeam.removePlayer(player);

		oppenentsPlayerTeam.addPlayer(player);
	}

	public void addPlayerAlliesPlayers(Player player, Team oppenentsPlayerTeam, Team alliesPlayerTeam,
			Team defaultPlayerTeam) {
		oppenentsPlayerTeam.removePlayer(player);
		defaultPlayerTeam.removePlayer(player);

		alliesPlayerTeam.addPlayer(player);
	}

	public void addPlayerDefaultPlayers(Player player, Team oppenentsPlayerTeam, Team alliesPlayerTeam,
			Team defaultPlayerTeam) {
		oppenentsPlayerTeam.removePlayer(player);
		alliesPlayerTeam.removePlayer(player);

		defaultPlayerTeam.addPlayer(player);
	}

	public ScoreboardCreator getScoreboardCreator() {
		return this.scoreboardCreator;
	}

	public PracticePlayer getPracticePlayer() {
		return this.practicePlayer;
	}

	public void removeText(int line) {
		this.scoreboardCreator.clearText(line);
	}

	public void clearAllText() {
		for (int i = 0; i < 15; i++) {
			removeText(i);
		}
	}
}
