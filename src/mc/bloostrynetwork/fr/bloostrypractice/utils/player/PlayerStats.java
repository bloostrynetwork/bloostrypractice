package mc.bloostrynetwork.fr.bloostrypractice.utils.player;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;

public class PlayerStats {

	private String uuid;

	private boolean scoreboard = true;
	
	private int playGames;
	private int loseGames;
	private int winGame;

	public HashMap<GameType, Integer> elos = new HashMap<>();

	private File file;

	private PlayerStats(String uuid) {
		this.uuid = uuid;
		this.playGames = 0;
		this.loseGames = 0;
		this.winGame = 0;

		for (GameType gameType : GameType.values()) {
			elos.put(gameType, 1000);
		}

		createFile();
		save();
	}

	public void save() {
		deleteFile();
		createFile();
		try {
			PrintWriter printWriter = new PrintWriter(new FileOutputStream(this.file), true);
			printWriter.println(Main.bloostryAPI.getGsonInstance().toJson(this));
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void createFile() {
		file = new File(BloostryPractice.STATS_FOLDER.getAbsolutePath() +  "/" + this.uuid + ".json");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteFile() {
		if (file.exists())
			file.delete();
	}

	public String getUuid() {
		return uuid;
	}

	public int getPlayGames() {
		return playGames;
	}

	public int getLoseGames() {
		return loseGames;
	}

	public int getWinGame() {
		return winGame;
	}

	public HashMap<GameType, Integer> getElos() {
		return elos;
	}

	public Integer getElo(GameType gameType) {
		if (!elos.containsKey(gameType))
			elos.put(gameType, 1000);
		return elos.get(gameType);
	}

	public Integer getGlobalElo() {
		int globalElo = 0;

		for (GameType gameType : GameType.values()) {
			globalElo += getElo(gameType);
		}
		return globalElo / GameType.values().length;
	}

	public static PlayerStats getPlayerStats(String uuid) {
		File file = new File(BloostryPractice.STATS_FOLDER.getAbsolutePath() + "/" + uuid + ".json");
		if (file.exists()) {
			try {
				return (PlayerStats) Main.bloostryAPI.getGsonInstance().fromJson(new FileReader(file), PlayerStats.class);
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (JsonIOException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void create(String uuid) {
		new PlayerStats(uuid);
	}
	
	public boolean hasScoreboard() {
		return this.scoreboard;
	}
	
	public void setScoreboard(boolean scoreboard) {
		this.scoreboard = scoreboard;
		save();
	}

	public void setElo(GameType gameType, int newElo) {
		elos.put(gameType, newElo);
		save();
	}
}
