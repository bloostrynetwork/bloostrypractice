package mc.bloostrynetwork.fr.bloostrypractice.utils.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class Inventory {

	private org.bukkit.inventory.Inventory inventory;

	public Inventory(Player player, String title, Integer slot){
		this.inventory = Bukkit.createInventory(player, slot, title);
	}

	public Inventory(Player player, String title, InventoryType inventoryType){
		this.inventory = Bukkit.createInventory(player, inventoryType, title);
	}

	public void setContents(ItemStack[] contents){
		this.inventory.setContents(contents);
	}
	
	public void setItem(ItemStack itemStack, Integer slot){
		this.inventory.setItem(slot, itemStack);
	}
	
	public void addItem(ItemStack itemStack){
		this.inventory.addItem(itemStack);
	}
	
	public void removeItem(ItemStack itemStack){
		this.inventory.removeItem(itemStack);
	}
	
	public void clear(){
		this.inventory.clear();
	}
	
	public ItemStack[] getContents(){
		return this.inventory.getContents();
	}
	
	public String getTitle(){
		return this.inventory.getTitle();
	}
	
	public org.bukkit.inventory.Inventory toInventory(){
		return this.inventory;
	}
}





