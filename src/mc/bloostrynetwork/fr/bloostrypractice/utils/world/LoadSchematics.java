package mc.bloostrynetwork.fr.bloostrypractice.utils.world;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.world.DataException;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;

public class LoadSchematics {
	private static File schematic;

	public static void loadSchematic(World world, String schematicName) {
		EditSession es = new EditSession(new BukkitWorld(world), 999999999);

		schematic = new File(getWorldEdit().getDataFolder(), "schematics/" + schematicName+".schematic");
		if (!schematic.exists()) {
			Bukkit.getPluginManager().disablePlugin(BloostryPractice.getInstance());
		}
		try {
			loadArena(es, schematic, new Vector(0, 168, 0));
		} catch (MaxChangedBlocksException | DataException | IOException e) {
			e.printStackTrace();
		}
	}

	public static WorldEditPlugin getWorldEdit() {
		Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
		if ((plugin == null) || (!(plugin instanceof WorldEditPlugin))) {
			return null;
		}
		return (WorldEditPlugin) plugin;
	}

	private static List<Integer> getIntegerList(int start) {
		ArrayList<Integer> integerList = new ArrayList<>();
		for (int i = start; i != 0; i--) {
		}
		return integerList;
	}

	public static void loadArena(EditSession es, File file, Vector origin)
			throws DataException, IOException, MaxChangedBlocksException {
		CuboidClipboard cc = CuboidClipboard.loadSchematic(file);
		cc.paste(es, origin, false);
	}
}
