package mc.bloostrynetwork.fr.bloostrypractice.utils.game;

public class GameElo
{
  private static double[] getEstimations(double rankingA, double rankingB)
  {
    double[] ret = new double[2];
    double estA = 1.0D / (1.0D + Math.pow(10.0D, (rankingB - rankingA) / 400.0D));
    double estB = 1.0D / (1.0D + Math.pow(10.0D, (rankingA - rankingB) / 400.0D));
    ret[0] = estA;
    ret[1] = estB;
    return ret;
  }
  
  private static int getConstant(int ranking)
  {
    if (ranking < 2000) {
      return 32;
    }
    if (ranking < 2401) {
      return 24;
    }
    return 16;
  }
  
  public static int[] getNewRankings(int rankingA, int rankingB, boolean victoryA)
  {
    double[] ests = new double[2];
    int[] ret = new int[2];
    ests = getEstimations(rankingA, rankingB);
    int newRankA = (int)(rankingA + getConstant(rankingA) * ((victoryA ? 1 : 0) - ests[0]));
    int newRankB = (int)(rankingB + getConstant(rankingB) * ((victoryA ? 0 : 1) - ests[1]));
    ret[0] = Math.round(newRankA);
    ret[1] = Math.round(newRankB);
    return ret;
  }
}
