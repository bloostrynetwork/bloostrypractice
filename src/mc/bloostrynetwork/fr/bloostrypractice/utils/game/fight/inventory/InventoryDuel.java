package mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;

public class InventoryDuel {
	
	public static Map<Player, InventoryDuel> inventorys = new HashMap<>();
	
	private int id;
	private Player player;
	private Inventory gui;
	private ItemStack[] armors;
	private ItemStack[] inventory;
	private int time = 60;

	public InventoryDuel(List<PotionEffect> potions, int health, int foodLevel, Player player) {
		this.player = player;
		this.armors = player.getInventory().getArmorContents();
		this.inventory = player.getInventory().getContents();
		String name = "�cInventaire - �f" + player.getName();
		if (name.length() >= 32) {
			name = name.substring(0, 32);
		}
		this.gui = Bukkit.createInventory(null, 54, name);

		this.gui.setContents(this.inventory);
		for (int i = 36; i < 45; i++) {
			this.gui.setItem(i, new Item(Material.STAINED_GLASS_PANE, (byte) 5, "�a.", null, null).toItemStack());
		}
		this.gui.setItem(45, this.armors[0]);
		this.gui.setItem(46, this.armors[1]);
		this.gui.setItem(47, this.armors[2]);
		this.gui.setItem(48, this.armors[3]);

		List<String> lore = new ArrayList<>();
		lore.add("�cJoueur: �f" + player.getName());
		lore.add("�cVie: �f" + health);
		lore.add("�cFaim: �f" + foodLevel);
		lore.add("�cListe des �ffets de potions:");
		for (PotionEffect potion : potions) {
			lore.add("�f- �c" + potion.getType().getName() + " " + (potion.getAmplifier() + 1) + " �7(�6"
					+ DateUtils.getTimerDate((int) (potion.getDuration() / 20)) + "�7)");
		}
		this.gui.setItem(53, new Item(Material.PAPER, "�6Information", lore, null).toItemStack());
		inventorys.put(player, this);
		this.id = inventorys.size();
		//startCooldown();
	}

	public void openInventory(Player player) {
		player.openInventory(this.gui);
		player.sendMessage(PracticeManager.PREFIX + "�cOuverture de l'inventaire de �f"
				+ this.player.getName() + "�c.");
	}
	
	/*

	private void startCooldown() {
		Bukkit.getScheduler().runTaskLater(BloostryPractice.getInstance(), new Runnable() {
			public void run() {
				if ((InventoryDuel.inventorys.containsKey(InventoryDuel.this.player))
						&& (((InventoryDuel) InventoryDuel.inventorys.get(InventoryDuel.this.player))
								.getID() == InventoryDuel.this.getID())) {
					Player[] arrayOfPlayer;
					int j = (arrayOfPlayer = Bukkit.getOnlinePlayers()).length;
					for (int i = 0; i < j; i++) {
						Player players = arrayOfPlayer[i];
						if (players.getOpenInventory().getTitle()
								.equals("�cInventaire - �f" + InventoryDuel.this.player.getName() + "�c.")) {
							InventoryDuel.this.player.closeInventory();
						}
					}
					InventoryDuel.inventorys.remove(InventoryDuel.this.player);
				}
			}
		}, this.time * 20);
	}
	
	*/

	public Player getPlayer() {
		return this.player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public ItemStack[] getArmors() {
		return this.armors;
	}

	public void setArmors(ItemStack[] armors) {
		this.armors = armors;
	}

	public ItemStack[] getInventory() {
		return this.inventory;
	}

	public void setInventory(ItemStack[] inventory) {
		this.inventory = inventory;
	}

	public int getTime() {
		return this.time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public Inventory getGui() {
		return this.gui;
	}

	public void setGui(Inventory gui) {
		this.gui = gui;
	}

	public int getID() {
		return this.id;
	}

	public void setID(int id) {
		this.id = id;
	}
}
