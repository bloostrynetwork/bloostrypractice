package mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight;

public enum FightType {

	UNRANKED("Unranked", "�9Unkanked", true),
	RANKED("Ranked", "�9Ranked", true),
	DUEL("Duel", "�eDuel", false),
	TEAM_FIGHT("Team Fight", "�cTeam Fight", true);
	
	private String name;
	private String coloredName;
	private boolean hasQueue;
	
	FightType(String name, String coloredName, boolean hasQueue){
		this.name= name;
		this.coloredName= coloredName;
		this.hasQueue = hasQueue;
	}
	
	public String getName() {
		return this.name;
	}
	
	public boolean hasQueue() {
		return this.hasQueue;
	}
	
	public String getColoredName() {
		return this.coloredName;
	}
}
