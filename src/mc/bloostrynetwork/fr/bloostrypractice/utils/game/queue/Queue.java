package mc.bloostrynetwork.fr.bloostrypractice.utils.game.queue;

import java.util.ArrayList;

import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.FightType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class Queue {

	private FightType fightType;
	private GameType gameType;
	private ArrayList<PracticePlayer> playersInQueue = new ArrayList<>();	
	private ArrayList<PracticePlayer> playersInFight = new ArrayList<>();
	
	public Queue(FightType fightType, GameType gameType) {
		this.fightType = fightType;
		this.gameType = gameType;
	}
	
	public FightType getFightType() {
		return fightType;
	}
	
	public GameType getGameType() {
		return gameType;
	}
	
	public ArrayList<PracticePlayer> getPlayersInQueue() {
		return playersInQueue;
	}
	
	public ArrayList<PracticePlayer> getPlayersInFight() {
		return playersInFight;
	}
}
