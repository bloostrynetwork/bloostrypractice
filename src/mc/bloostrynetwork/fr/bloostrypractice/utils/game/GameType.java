package mc.bloostrynetwork.fr.bloostrypractice.utils.game;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits.BuildUHC;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits.Debuff;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits.IKit;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits.Nodebuff;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits.Rush;

public enum GameType {

	NODEBUFF(1, "Potion (Nodebuff)","�cPotion (Nodebuff)", new ItemStack(Material.getMaterial(373)),(byte) 16421, new Nodebuff(null)),
	DEBUFF(2, "Potion (Debuff)","�cPotion (Debuff)",  new ItemStack(Material.getMaterial(373)), (byte) 16388, new Debuff(null)),
	BUILDUHC(3, "BuildUHC","�cBuildUHC",  new ItemStack(Material.LAVA_BUCKET), (byte) 0, new BuildUHC(null)),
	RUSH(4, "Rush","�cRush",  new ItemStack(Material.BED), (byte) 0, new Rush(null))
	/*, GAPPLE(4, "GApple","�cGApple",  new ItemStack(Material.GOLDEN_APPLE),(byte)  0, null),*/;
	
	private int id;
	private String name;
	private String nameColored;
	private ItemStack icon;
	private byte data;
	private IKit ikit;

	GameType(int id, String name, String nameColored, ItemStack icon, byte data, IKit ikit){
		this.id = id;
		this.name = name;
		this.nameColored = nameColored;
		this.icon = icon;
		this.data = data;
		this.ikit = ikit;
		if(this.ikit != null) {
			this.ikit.setGameType(this);
		}
	}
	
	public int getID() {
		return this.id;
	}
	
	public String getColoredName() {
		return this.nameColored;
	}
	
	public ItemStack getIcon() {
		return this.icon;
	}
	
	public String getName() {
		return this.name;
	}

	public byte getData() {
		return this.data;
	}
	
	public IKit getIKit() {
		return this.ikit;
	}
}
