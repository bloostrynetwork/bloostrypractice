package mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.world.DataException;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.Cuboide;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.LoadSchematics;

public abstract class ArenaAbstract {

	private String name;
	private int id;
	private boolean used;
	private int[] firstLocation;
	private int[] secondLocation;
	private int[] generateLocation;
	private String cuboide;
	private String fight;
	protected File file;

	public ArenaAbstract(String name, Location generateLocation, Location firstLocation, Location secondLocation,
			Cuboide cuboide) {
		this.name = name;

		File folder = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + this.name + "/");
		if (folder.exists()) {
			int id = 0;
			File[] arrayOfFile;
			int j = (arrayOfFile = folder.listFiles()).length;
			for (int i = 0; i < j; i++) {
				File file = arrayOfFile[i];
				if ((file != null) && (file.exists())) {
					id++;
				}
			}
			this.id = (id + 1);
		} else {
			this.id = 1;
		}

		this.used = false;
		this.firstLocation = new int[] { firstLocation.getBlockX(), firstLocation.getBlockY(),
				firstLocation.getBlockZ(), (int) firstLocation.getYaw(), (int) firstLocation.getPitch() };
		this.secondLocation = new int[] { secondLocation.getBlockX(), secondLocation.getBlockY(),
				secondLocation.getBlockZ(), (int) secondLocation.getYaw(), (int) secondLocation.getPitch() };
		this.generateLocation = new int[] { generateLocation.getBlockX(), generateLocation.getBlockY(),
				generateLocation.getBlockZ(), (int) generateLocation.getYaw(), (int) generateLocation.getPitch() };

		this.cuboide = Main.bloostryAPI.getGsonInstance().toJson(cuboide);
		this.fight = null;

		createFile();
		save();
	}

	public abstract void save();

	public void createFile() {
		File folder = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + this.name + "/");
		if (!folder.exists()) {
			folder.mkdir();
		}
		this.file = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + this.name + "/" + this.name + "-"
				+ this.id + ".json");
		if (!this.file.exists()) {
			try {
				this.file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteFile() {
		if (this.file.exists()) {
			this.file.delete();
		}
	}

	public void clearArena() {
		Cuboide cuboide = getCuboide();
		World world = cuboide.getWorld();
		for (int y = cuboide.getMinY(); y <= cuboide.getMaxY(); y++) {
			Console.sendMessageDebug("�aY: " + y);
			for (int x = cuboide.getMinX(); x <= cuboide.getMaxX(); x++) {
				Console.sendMessageDebug("�9X: " + x);
				for (int z = cuboide.getMinZ(); z <= cuboide.getMaxZ(); z++) {
					Console.sendMessageDebug("�cZ: " + z);
					if(getFight().getGameType() == GameType.BUILDUHC) {
						Block block = world.getBlockAt(x, y, z);
						if ((block.getType().equals(Material.COBBLESTONE)) || (block.getType().equals(Material.WOOD))
								|| (block.getType().equals(Material.LAVA)) || (block.getType().equals(Material.WATER))
								|| (block.getType().equals(Material.STONE))
								|| (block.getType().equals(Material.STATIONARY_LAVA))
								|| (block.getType().equals(Material.STATIONARY_WATER))
								|| (block.getType().equals(Material.OBSIDIAN))
								|| (block.getType().equals(Material.WATER_LILY))) {
							block.setType(Material.AIR);
						}
					} else if (getFight().getGameType() == GameType.RUSH) {
						Block block = world.getBlockAt(x, y, z);
						if ((block.getType().equals(Material.SANDSTONE)) || (block.getType().equals(Material.FIRE))
								|| (block.getType().equals(Material.TNT))) {
							block.setType(Material.AIR);
						}
					}
				}
			}
		}
		if (getFight().getGameType() == GameType.RUSH) {
			Console.sendMessageDebug("�aStarted generation map ...");

			EditSession es = new EditSession(new BukkitWorld(getGenerationLocation().getWorld()), 999999999);

			File schematic = new File("plugins/" + BloostryPractice.getInstance().getDescription().getName()
					+ "/schematics/" + name + ".schematic");
			try {
				LoadSchematics.loadArena(es, schematic, new Vector(getGenerationLocation().getBlockX(),
						getGenerationLocation().getBlockY(), getGenerationLocation().getBlockZ()));
			} catch (MaxChangedBlocksException | DataException | IOException e) {
				e.printStackTrace();
				Console.sendMessageDebug("�c.");
			}
			Console.sendMessageDebug("�aGeneration map finished and with succes.");
		}
	}

	public boolean isUsed() {
		return this.used;
	}

	public void setUsed(boolean used) {
		this.used = used;
		save();
	}

	public Fight getFight() {
		return (Fight) Main.bloostryAPI.getGsonInstance().fromJson(this.fight, Fight.class);
	}

	public void setFight(Fight fight) {
		this.fight = Main.bloostryAPI.getGsonInstance().toJson(fight);
		save();
	}

	public String getName() {
		return this.name;
	}

	public int getId() {
		return this.id;
	}

	public Location getFirstLocation() {
		return new Location(PracticeManager.getInstance().getSpawnLocation().getWorld(), this.firstLocation[0],
				this.firstLocation[1], this.firstLocation[2], this.firstLocation[3], this.firstLocation[4]);
	}

	public Location getSecondLocation() {
		return new Location(PracticeManager.getInstance().getSpawnLocation().getWorld(), this.secondLocation[0],
				this.secondLocation[1], this.secondLocation[2], this.secondLocation[3], this.secondLocation[4]);
	}

	public Location getGenerationLocation() {
		return new Location(PracticeManager.getInstance().getSpawnLocation().getWorld(), this.generateLocation[0],
				this.generateLocation[1], this.generateLocation[2], this.generateLocation[3], this.generateLocation[4]);
	}

	public Cuboide getCuboide() {
		return (Cuboide) Main.bloostryAPI.getGsonInstance().fromJson(this.cuboide, Cuboide.class);
	}

	public File getFile() {
		return this.file;
	}

}
