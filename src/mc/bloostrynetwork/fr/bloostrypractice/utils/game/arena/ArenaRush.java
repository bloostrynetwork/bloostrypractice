package mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.bukkit.Location;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.world.DataException;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.Cuboide;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.LoadSchematics;

public class ArenaRush extends ArenaAbstract {

	private int[] bedLocation1;
	private int[] bedLocation2;
	
	public ArenaRush(String name, Location generateLocation, Location firstLocation, Location secondLocation,
			Cuboide cuboide, Location bedLocation1, Location bedLocation2) {
		super(name, generateLocation, firstLocation, secondLocation, cuboide);
		this.bedLocation1 = new int[] { bedLocation1.getBlockX(), bedLocation1.getBlockY(),
				bedLocation1.getBlockZ(), (int) bedLocation1.getYaw(), (int) bedLocation1.getPitch() };
		this.bedLocation2 = new int[] { bedLocation2.getBlockX(), bedLocation2.getBlockY(),
				bedLocation2.getBlockZ(), (int) bedLocation2.getYaw(), (int) bedLocation2.getPitch() };
		save();
	}

	public void save() {
		deleteFile();
		createFile();
		try {
			PrintWriter printWriter = new PrintWriter(new FileOutputStream(this.file), true);
			Console.sendMessageDebug("bedLocation1 -> "+bedLocation1);
			Console.sendMessageDebug("bedLocation2 -> "+bedLocation2);
			String jsonMessage = Main.bloostryAPI.getGsonInstance().toJson(ArenaRush.this);
			printWriter.println(jsonMessage);
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Console.sendMessageDebug("Saving json file for arena -> "+this.getName());
	}

	public static ArenaRush getArena(String name, int id) {
		File folder = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + name + "/");
		if (folder.exists()) {
			File file = new File(
					BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + name + "/" + name + "-" + id + ".json");
			try {
				return (ArenaRush) new Gson().fromJson(new FileReader(file), ArenaRush.class);
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (JsonIOException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			Console.sendMessageError("�cThe folder not existed !");
		}
		return null;
	}

	public static ArenaRush getLastArena(String name) {
		File folder = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + name + "/");
		if (folder.exists()) {
			int id = 0;
			File[] arrayOfFile;
			int j = (arrayOfFile = folder.listFiles()).length;
			for (int i = 0; i < j; i++) {
				File file = arrayOfFile[i];
				if ((file != null) && (file.exists())) {
					id++;
				}
			}
			return getArena(name, id);
		}
		Console.sendMessageError("�cThe folder not existed !");

		return null;
	}

	public static void createArena(String name, Location generateLocation, Location location1, Location location2,
			Cuboide cuboide, Location bedLocation1, Location bedLocation2) {
		Console.sendMessageDebug("�aStarted generation map ...");

		EditSession es = new EditSession(new BukkitWorld(generateLocation.getWorld()), 999999999);

		File schematic = new File("plugins/" + BloostryPractice.getInstance().getDescription().getName()
				+ "/schematics/" + name + ".schematic");
		try {
			LoadSchematics.loadArena(es, schematic, new Vector(generateLocation.getBlockX(),
					generateLocation.getBlockY(), generateLocation.getBlockZ()));
		} catch (MaxChangedBlocksException | DataException | IOException e) {
			e.printStackTrace();
			Console.sendMessageDebug("�c.");
		}
		Console.sendMessageDebug("�aGeneration map finished and with succes.");
		new ArenaRush(name, generateLocation, location1, location2, cuboide, bedLocation1, bedLocation2);
	}

	public static void createArenaAuto(String name) {
		int spacer = 500;
		ArenaRush arena = getLastArena(name);
		if (arena != null) {
			Location location1 = arena.getFirstLocation().clone().add(0.0D, 0.0D, spacer);
			Location location2 = arena.getSecondLocation().clone().add(0.0D, 0.0D, spacer);
			Location cuboideLocation1 = arena.getCuboide().getLocation1().clone().add(0.0D, 0.0D, spacer);
			Location cuboideLocation2 = arena.getCuboide().getLocation2().clone().add(0.0D, 0.0D, spacer);

			Location generateLocation = arena.getGenerationLocation().clone().add(0.0D, 0.0D, spacer);
			
			Location bedLocation1 = arena.getCuboide().getLocation1().clone().add(0.0D, 0.0D, spacer);
			Location bedLocation2 = arena.getCuboide().getLocation2().clone().add(0.0D, 0.0D, spacer);

			Cuboide cuboide = new Cuboide(cuboideLocation1, cuboideLocation2);

			createArena(name, generateLocation, location1, location2, cuboide, bedLocation1, bedLocation2);
		} else {
			Console.sendMessageError("�cThis don't existed !");
		}
	}

	public Location getBedLocation1() {
		return new Location(PracticeManager.getInstance().getSpawnLocation().getWorld(), this.bedLocation1[0],
				this.bedLocation1[1], this.bedLocation1[2], this.bedLocation1[3], this.bedLocation1[4]);
	}

	public Location getBedLocation2() {
		return new Location(PracticeManager.getInstance().getSpawnLocation().getWorld(), this.bedLocation2[0],
				this.bedLocation2[1], this.bedLocation2[2], this.bedLocation2[3], this.bedLocation2[4]);
	}
}
