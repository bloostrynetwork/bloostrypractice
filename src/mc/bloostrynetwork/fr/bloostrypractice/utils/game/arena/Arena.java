package mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.bukkit.Location;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.world.DataException;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.Cuboide;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.LoadSchematics;

public class Arena extends ArenaAbstract {
	
	public Arena(String name, Location generateLocation, Location firstLocation, Location secondLocation,
			Cuboide cuboide) {
		super(name, generateLocation, firstLocation, secondLocation, cuboide);
	}

	public void save() {
		deleteFile();
		createFile();
		try {
			PrintWriter printWriter = new PrintWriter(new FileOutputStream(file), true);
			String jsonMessage = Main.bloostryAPI.getGsonInstance().toJson(this);
			printWriter.println(jsonMessage);
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Arena getArena(String name, int id) {
		File folder = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + name + "/");
		if (folder.exists()) {
			File file = new File(
					BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + name + "/" + name + "-" + id + ".json");
			try {
				return (Arena) new Gson().fromJson(new FileReader(file), Arena.class);
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (JsonIOException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			Console.sendMessageError("�cThe folder not existed !");
		}
		return null;
	}

	public static Arena getLastArena(String name) {
		File folder = new File(BloostryPractice.ARENA_FOLDER.getAbsolutePath() + "/" + name + "/");
		if (folder.exists()) {
			int id = 0;
			File[] arrayOfFile;
			int j = (arrayOfFile = folder.listFiles()).length;
			for (int i = 0; i < j; i++) {
				File file = arrayOfFile[i];
				if ((file != null) && (file.exists())) {
					id++;
				}
			}
			return getArena(name, id);
		}
		Console.sendMessageError("�cThe folder not existed !");

		return null;
	}

	public static void createArena(String name, Location generateLocation, Location location1, Location location2,
			Cuboide cuboide) {
		Console.sendMessageDebug("�aStarted generation map ...");

		EditSession es = new EditSession(new BukkitWorld(generateLocation.getWorld()), 999999999);

		File schematic = new File("plugins/" + BloostryPractice.getInstance().getDescription().getName()
				+ "/schematics/" + name + ".schematic");
		try {
			LoadSchematics.loadArena(es, schematic, new Vector(generateLocation.getBlockX(),
					generateLocation.getBlockY(), generateLocation.getBlockZ()));
		} catch (MaxChangedBlocksException | DataException | IOException e) {
			e.printStackTrace();
			Console.sendMessageDebug("�c.");
		}
		Console.sendMessageDebug("�aGeneration map finished and with succes.");
		new Arena(name, generateLocation, location1, location2, cuboide);
	}

	public static void createArenaAuto(String name) {
		int spacer = 500;
		Arena arena = getLastArena(name);
		if (arena != null) {
			Location location1 = arena.getFirstLocation().clone().add(0.0D, 0.0D, spacer);
			Location location2 = arena.getSecondLocation().clone().add(0.0D, 0.0D, spacer);
			Location cuboideLocation1 = arena.getCuboide().getLocation1().clone().add(0.0D, 0.0D, spacer);
			Location cuboideLocation2 = arena.getCuboide().getLocation2().clone().add(0.0D, 0.0D, spacer);

			Location generateLocation = arena.getGenerationLocation().clone().add(0.0D, 0.0D, spacer);

			Cuboide cuboide = new Cuboide(cuboideLocation1, cuboideLocation2);

			createArena(name, generateLocation, location1, location2, cuboide);
		} else {
			Console.sendMessageError("�cThis don't existed !");
		}
	}
}
