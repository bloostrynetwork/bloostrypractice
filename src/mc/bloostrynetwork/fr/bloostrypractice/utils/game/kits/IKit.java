package mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;

public abstract class IKit {
	
	private GameType gameType;
	
	public IKit(GameType gameType) {
		this.gameType = gameType;
	}
	
	public IKit() {
	}

	public void save(Player player) {
		File file = new File(BloostryPractice.KITS_FOLDER.getAbsolutePath() + "/" + player.getName() + ".yml");
		if(!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		FileConfiguration fileconfiguration = YamlConfiguration.loadConfiguration(file);
		Console.sendMessageDebug(fileconfiguration+" / "+player+" / "+player.getInventory()+" / "+player.getInventory().getArmorContents() +" / "+getGameType() +" / "+gameType);
		fileconfiguration.set(getGameType().getName() + ".armor",
				player.getInventory().getArmorContents());
		fileconfiguration.set(getGameType().getName() + ".inventory",
				player.getInventory().getContents());
		try {
			fileconfiguration.save(file);
		} catch (IOException error) {
			error.printStackTrace();
		}
	}

	public boolean hasKitEdited(Player player) {
		File file = new File(BloostryPractice.KITS_FOLDER.getAbsolutePath() + "/" + player.getName() + ".yml");
		if(!file.exists())
			return false;
		FileConfiguration fileconfiguration = YamlConfiguration.loadConfiguration(file);
		if(!(fileconfiguration.contains(getGameType().getName()+".inventory")))
			return false;
		if(!(fileconfiguration.contains(getGameType().getName()+".armor")))
			return false;
		return true;
	}

	public void giveKitEdited(Player player) {
		File file = new File(BloostryPractice.KITS_FOLDER.getAbsolutePath() + "/" + player.getName()+".yml");
		FileConfiguration fileconfiguration = YamlConfiguration.loadConfiguration(file);
		ItemStack[] content = (ItemStack[]) ((Collection) fileconfiguration
				.get(getGameType().getName()+".inventory")).toArray(new ItemStack[0]);
		player.getInventory().setContents(content);
		player.updateInventory();
		content = (ItemStack[]) ((Collection) fileconfiguration.get(getGameType().getName()+".armor"))
				.toArray(new ItemStack[0]);
		player.getInventory().setArmorContents(content);
		player.updateInventory();
	}

	public abstract void giveKitDefault(Player player); 
	
	public GameType getGameType() {
		return this.gameType;
	}
	
	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}
}
