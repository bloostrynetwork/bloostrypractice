package mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;

public class Rush extends IKit {

	public Rush(GameType gameType) {
		super(gameType);
	}

	public Rush() {
	}

	@Override
	public void giveKitDefault(Player player) {
		ItemStack sword = new ItemStack(Material.STONE_SWORD);
	    ItemMeta swordMeta = sword.getItemMeta();
	    swordMeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
	    sword.setItemMeta(swordMeta);
	    
	    ItemStack golden_appel = new ItemStack(Material.GOLDEN_APPLE, 6);
	    
	    ItemStack iron_pickaxe = new ItemStack(Material.IRON_PICKAXE);
	    ItemMeta iron_pickaxe_meta = iron_pickaxe.getItemMeta();
	    iron_pickaxe_meta.addEnchant(Enchantment.DIG_SPEED, 3, true);
	    iron_pickaxe.setItemMeta(iron_pickaxe_meta);
	    
	    ItemStack sandstone = new ItemStack(Material.SANDSTONE, 64);

	    ItemStack tnt = new ItemStack(Material.TNT, 6);
	    ItemStack flint_and_steel = new ItemStack(Material.FLINT_AND_STEEL, 1);
	    flint_and_steel.setDurability((short) 10);
	    
	    Item helmet = new Item(Material.LEATHER_HELMET, null, null, null);
	    helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
	    
	    Item chestplate = new Item(Material.LEATHER_CHESTPLATE, null, null, null);
	    chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
	    
	    Item leggings = new Item(Material.LEATHER_LEGGINGS, null, null, null);
	    leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
	    
	    Item boots = new Item(Material.LEATHER_BOOTS, null, null, null);
	    boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
	    
	    Inventory inventory = player.getInventory();
	    
	    player.getEquipment().setHelmet(helmet.toItemStack());
	    player.getEquipment().setChestplate(chestplate.toItemStack());
	    player.getEquipment().setLeggings(leggings.toItemStack());
	    player.getEquipment().setBoots(boots.toItemStack());

	    inventory.setItem(0, sword);
	    inventory.setItem(1, iron_pickaxe);
	    inventory.setItem(2, tnt);
	    inventory.setItem(3, flint_and_steel);
	    inventory.setItem(4, golden_appel);
	    inventory.setItem(5, sandstone);
	    inventory.setItem(6, sandstone);
	    inventory.setItem(7, sandstone);
	    inventory.setItem(8, sandstone);
	    
	    player.updateInventory();
	}
}
