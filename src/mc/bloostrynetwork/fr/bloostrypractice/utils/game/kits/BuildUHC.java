package mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;

public class BuildUHC extends IKit {

	public BuildUHC(GameType gameType) {
		super(gameType);
	}

	@Override
	public void giveKitDefault(Player player) {
		ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
	    ItemMeta swordMeta = sword.getItemMeta();
	    swordMeta.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
	    sword.setItemMeta(swordMeta);
	    
	    ItemStack bow = new ItemStack(Material.BOW);
	    ItemMeta bowMeta = bow.getItemMeta();
	    bowMeta.addEnchant(Enchantment.ARROW_DAMAGE, 3, true);
	    bow.setItemMeta(bowMeta);
	    
	    ItemStack rod = new ItemStack(Material.FISHING_ROD);
	    
	    ItemStack lava_bucket = new ItemStack(Material.LAVA_BUCKET);
	    ItemStack water_bucket = new ItemStack(Material.WATER_BUCKET);
	    
	    ItemStack golden_appel = new ItemStack(Material.GOLDEN_APPLE, 6);
	    
	    ItemStack golden_head = new Item(Material.GOLDEN_APPLE, 3, "�6Golden Head", null, null).toItemStack();
	    
	    ItemStack diamond_pickaxe = new ItemStack(Material.DIAMOND_PICKAXE);
	    ItemStack diamond_axe = new ItemStack(Material.DIAMOND_AXE);
	    
	    ItemStack cobblestone = new ItemStack(Material.COBBLESTONE, 64);
	    ItemStack wood = new ItemStack(Material.WOOD, 64);
	    
	    ItemStack steak = new ItemStack(Material.COOKED_BEEF, 64);
	    
	    ItemStack arrow = new ItemStack(Material.ARROW, 32);
	    
	    Item helmet = new Item(Material.DIAMOND_HELMET, null, null, null);
	    helmet.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2, false);
	    
	    Item chestplate = new Item(Material.DIAMOND_CHESTPLATE, null, null, null);
	    chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
	    
	    Item leggings = new Item(Material.DIAMOND_LEGGINGS, null, null, null);
	    leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
	    
	    Item boots = new Item(Material.DIAMOND_BOOTS, null, null, null);
	    boots.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2, false);
	    
	    Inventory inventory = player.getInventory();
	    inventory.setItem(0, sword);
	    inventory.setItem(1, rod);
	    inventory.setItem(2, bow);
	    inventory.setItem(3, water_bucket);
	    inventory.setItem(4, lava_bucket);
	    inventory.setItem(5, lava_bucket);
	    inventory.setItem(6, golden_appel);
	    inventory.setItem(7, golden_head);
	    inventory.setItem(8, cobblestone);
	    inventory.setItem(9, steak);
	    inventory.setItem(10, wood);
	    inventory.setItem(11, water_bucket);
	    inventory.setItem(12, diamond_pickaxe);
	    inventory.setItem(13, diamond_axe);
	    inventory.setItem(14, arrow);
	    
	    player.getEquipment().setHelmet(helmet.toItemStack());
	    player.getEquipment().setChestplate(chestplate.toItemStack());
	    player.getEquipment().setLeggings(leggings.toItemStack());
	    player.getEquipment().setBoots(boots.toItemStack());
	    
	    player.updateInventory();
	}
}
