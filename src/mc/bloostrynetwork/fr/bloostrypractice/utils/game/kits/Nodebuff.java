package mc.bloostrynetwork.fr.bloostrypractice.utils.game.kits;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;

public class Nodebuff extends IKit {

	public Nodebuff(GameType gameType) {
		super(gameType);
	}

	@Override
	public void giveKitDefault(Player p) {
		p.setGameMode(GameMode.SURVIVAL);
		p.getEquipment().setArmorContents(null);
		p.getInventory().clear();

		ItemStack casque = new ItemStack(Material.DIAMOND_HELMET);
		casque.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemStack plastron = new ItemStack(Material.DIAMOND_CHESTPLATE);
		plastron.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemStack pantalon = new ItemStack(Material.DIAMOND_LEGGINGS);
		pantalon.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemStack bote = new ItemStack(Material.DIAMOND_BOOTS);
		bote.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemStack epee = new ItemStack(Material.DIAMOND_SWORD);
		epee.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		epee.addEnchantment(Enchantment.FIRE_ASPECT, 1);
		ItemStack fire = new ItemStack(Material.getMaterial(373), 1, (short) 8259);
		ItemStack pearl = new ItemStack(Material.ENDER_PEARL, 16);
		ItemStack speed = new ItemStack(Material.POTION, 1, (short) 34);
		ItemStack viande = new ItemStack(Material.COOKED_BEEF, 64);
		ItemStack healt = new ItemStack(Material.getMaterial(373), 1, (short) 16421);
		bote.addEnchantment(Enchantment.DURABILITY, 3);
		pantalon.addEnchantment(Enchantment.DURABILITY, 3);
		plastron.addEnchantment(Enchantment.DURABILITY, 3);
		casque.addEnchantment(Enchantment.DURABILITY, 3);

		p.getInventory().setHelmet(casque);
		p.getInventory().setChestplate(plastron);
		p.getInventory().setLeggings(pantalon);
		p.getInventory().setBoots(bote);
		p.getInventory().setItem(0, epee);
		p.getInventory().setItem(1, pearl);
		p.getInventory().setItem(8, speed);
		p.getInventory().setItem(7, fire);
		p.getInventory().setItem(9, speed);
		p.getInventory().setItem(18, speed);
		p.getInventory().setItem(27, speed);
		p.getInventory().setItem(2, viande);
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
		p.getInventory().addItem(new ItemStack[] { healt });
	}
}
