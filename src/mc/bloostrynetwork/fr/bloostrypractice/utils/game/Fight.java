package mc.bloostrynetwork.fr.bloostrypractice.utils.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.EnderPearlsPatch;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaAbstract;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaRush;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.FightType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.inventory.InventoryDuel;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;
import net.minecraft.server.v1_7_R4.ChatSerializer;
import net.minecraft.server.v1_7_R4.IChatBaseComponent;
import net.minecraft.server.v1_7_R4.PacketPlayOutChat;

public class Fight {

	public List<String> players;
	public List<String> oppenents;

	private GameType gameType;
	private FightType fightType;
	private ArenaAbstract arena;

	private long startGame;

	private boolean gameFinished = false;

	public Fight(List<String> players, List<String> oppenents, ArenaAbstract arena, GameType gameType,
			FightType fightType) {
		this.players = players;
		this.oppenents = oppenents;
		this.arena = arena;
		this.gameType = gameType;
		this.fightType = fightType;
	}

	public Fight(Player player, Player oppenent, ArenaAbstract arena, GameType gameType, FightType fightType) {
		this.players = new ArrayList<>();
		this.oppenents = new ArrayList<>();
		this.players.add(player.getName());
		this.oppenents.add(oppenent.getName());
		this.arena = arena;
		this.gameType = gameType;
		this.fightType = fightType;
	}

	public void start() {
		if (!(arena.isUsed())) {
			gameFinished = false;

			arena.setUsed(true);
			arena.setFight(this);

			this.startGame = System.currentTimeMillis();
			Item defaultKit = new Item(Material.PAPER, "�cKit D�faut", null, null);
			Item editKit = new Item(Material.PAPER, "�cKit Modifi�", null, null);

			for (PracticePlayer practicePlayers : getPlayersInFight()) {
				if (practicePlayers.isOnline()) {
					Player player = practicePlayers.getPlayer();
					practicePlayers.clearStuff();
					practicePlayers.setFight(this);
					player.getInventory().setItem(0, defaultKit.toItemStack());
					player.getInventory().setItem(2, editKit.toItemStack());
					player.updateInventory();
				}
			}

			for (String playerName : this.players) {
				PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(playerName);
				if (practicePlayer.isOnline()) {
					practicePlayer.setPlayerStatus(PlayerStatus.INFIGHT);
					Player player = practicePlayer.getPlayer();
					player.teleport(arena.getFirstLocation());
					player.setGameMode(GameMode.SURVIVAL);
					PracticeManager.getInstance().getQueue(fightType, gameType).getPlayersInFight().add(practicePlayer);
				} else {
					this.players.remove(playerName);
					sendMessage(PracticeManager.PREFIX + "�e" + playerName + " �fest mort en d�connectant !");
				}
			}

			for (String playerName : this.oppenents) {
				PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(playerName);
				if (practicePlayer.isOnline()) {
					Player player = practicePlayer.getPlayer();
					practicePlayer.setPlayerStatus(PlayerStatus.INFIGHT);
					player.teleport(arena.getSecondLocation());
					player.setGameMode(GameMode.SURVIVAL);
					PracticeManager.getInstance().getQueue(fightType, gameType).getPlayersInFight().add(practicePlayer);
				} else {
					this.oppenents.remove(playerName);
					sendMessage(PracticeManager.PREFIX + "�e" + playerName + " �fest mort en d�connectant !");
				}
			}

			sendMessage(PracticeManager.PREFIX + "�fLancement du combat que le meilleur gagne.");
		} else {
			sendMessage(PracticeManager.PREFIX + "�cErreur, cette ar�ne est actuellement utilis�e !");
		}
	}

	public void stop(Player playerLose) {
		gameFinished = true;

		Long timeElapsed = getTimeElapsed();

		PracticePlayer practicePlayerWinner = PracticeManager.getInstance().getPracticePlayer(getOppenent(playerLose));
		PracticePlayer practicePlayerLoser = PracticeManager.getInstance().getPracticePlayer(playerLose.getName());
		PracticeManager.getInstance().getQueue(practicePlayerWinner).getPlayersInFight().remove(practicePlayerWinner);

		List<PotionEffect> winnerPotions = new ArrayList<>();
		for (PotionEffect potions : practicePlayerWinner.getPlayer().getActivePotionEffects()) {
			if (potions != null) {
				winnerPotions.add(potions);
			}
		}
		new InventoryDuel(winnerPotions, (int) ((Damageable) practicePlayerWinner.getPlayer()).getHealth(),
				practicePlayerWinner.getPlayer().getFoodLevel(), practicePlayerWinner.getPlayer());

		IChatBaseComponent c = ChatSerializer.a("{\"text\":\"  �f- " + playerLose.getName()
				+ "\",\"color\":\"gold\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "
				+ playerLose.getName() + "\"}}");
		PacketPlayOutChat packet = new PacketPlayOutChat(c);

		IChatBaseComponent c2 = ChatSerializer.a("{\"text\":\"  �f- " + practicePlayerWinner.getPlayer().getName()
				+ "\",\"color\":\"gold\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "
				+ practicePlayerWinner.getPlayer().getName() + "\"}}");
		PacketPlayOutChat packet2 = new PacketPlayOutChat(c2);

		int winnerElo = practicePlayerWinner.getPlayerStats().getElo(gameType);
		int loserElo = practicePlayerLoser.getPlayerStats().getElo(gameType);

		int newWinnerElo = GameElo.getNewRankings(winnerElo, loserElo, true)[0];
		int newLoserElo = GameElo.getNewRankings(winnerElo, loserElo, true)[1];

		sendMessage("�7�m--------------------------");
		sendMessage("");
		sendMessage("�cGagnant: �f" + practicePlayerWinner.getPlayer().getName());
		sendMessage("�cPerdant: �f" + playerLose.getName());
		sendMessage("�cMap: �f" + arena.getName() + " �7#�e" + arena.getId());
		sendMessage("�cTemps de jeu: �f" + DateUtils.getTimerDate(timeElapsed));
		sendMessage("�cMode de jeu: �f" + gameType.getName());
		if (fightType.equals(FightType.RANKED)) {
			practicePlayerWinner.getPlayer()
					.sendMessage("�cElo: �a+" + (newWinnerElo - winnerElo) + " �7(" + newWinnerElo + ")");
			playerLose.getPlayer().sendMessage("�cElo: �c-" + (loserElo - newLoserElo) + " �7(" + newLoserElo + ")");
			practicePlayerWinner.getPlayerStats().setElo(gameType, newWinnerElo);
			practicePlayerLoser.getPlayerStats().setElo(gameType, newLoserElo);
		}
		sendMessage("�cInventaire:");

		((CraftPlayer) practicePlayerWinner.getPlayer()).getHandle().playerConnection.sendPacket(packet);
		((CraftPlayer) practicePlayerWinner.getPlayer()).getHandle().playerConnection.sendPacket(packet2);
		((CraftPlayer) playerLose).getHandle().playerConnection.sendPacket(packet);
		((CraftPlayer) playerLose).getHandle().playerConnection.sendPacket(packet2);

		sendMessage("");
		sendMessage("�7�m--------------------------");

		Bukkit.getScheduler().runTaskLater(BloostryPractice.getInstance(), new Runnable() {
			@Override
			public void run() {
				practicePlayerWinner.clearStuff();
				practicePlayerWinner.clearAllEffects();
				practicePlayerWinner.joinInSpawn();
				practicePlayerWinner.setFight(null);
				practicePlayerWinner.setPlayerStatus(PlayerStatus.INSPAWN);
				practicePlayerWinner.getPlayer().teleport(PracticeManager.getInstance().getSpawnLocation());

				EnderPearlsPatch.timers.remove(practicePlayerWinner.getPlayer());
				practicePlayerWinner.getPlayer().setLevel(0);

				arena.clearArena();

				arena.setFight(null);
				arena.setUsed(false);
			}
		}, 30);
	}

	public void death(String killer, Player player) {
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		if (practicePlayer.isOnline()) {
			if (gameType == GameType.RUSH) {
				if (!(isFightTeam())) {
					// QUE LE JEUX RUSH ICI
					ArenaRush arenaRush = (ArenaRush) arena;

					if (killer.equalsIgnoreCase("damage") || killer.equalsIgnoreCase("void")) {
						Console.sendMessageDebug(arenaRush.getBedLocation1().getBlock().getType()+" / "+arenaRush.getBedLocation2().getBlock().getType());
						if (Fight.this.players.contains(player.getName())) {
							// LOC 1
							
							if (arenaRush.getBedLocation1().getBlock().getType() != Material.BED
									&& arenaRush.getBedLocation1().getBlock().getType() != Material.BED_BLOCK) {
								// FINISH GAME
								return;
							}

							practicePlayer.clearStuff();

							Item defaultKit = new Item(Material.PAPER, "�cKit D�faut", null, null);
							Item editKit = new Item(Material.PAPER, "�cKit Modifi�", null, null);
							player.getInventory().setItem(0, defaultKit.toItemStack());
							player.getInventory().setItem(2, editKit.toItemStack());
							player.updateInventory();
						} else if (Fight.this.oppenents.contains(player.getName())) {
							// LOC 2
							if (arenaRush.getBedLocation2().getBlock().getType() != Material.BED
									&& arenaRush.getBedLocation2().getBlock().getType() != Material.BED_BLOCK) {
								// FINISH GAME
								return;
							}

							practicePlayer.clearStuff();

							Item defaultKit = new Item(Material.PAPER, "�cKit D�faut", null, null);
							Item editKit = new Item(Material.PAPER, "�cKit Modifi�", null, null);
							player.getInventory().setItem(0, defaultKit.toItemStack());
							player.getInventory().setItem(2, editKit.toItemStack());
							player.updateInventory();
						}
					} else if (killer.equalsIgnoreCase("deconnection")){
						PracticeManager.getInstance().getQueue(practicePlayer).getPlayersInFight()
								.remove(practicePlayer);

						List<PotionEffect> loserPotions = new ArrayList<>();
						for (PotionEffect potions : player.getActivePotionEffects()) {
							if (potions != null) {
								loserPotions.add(potions);
							}
						}

						practicePlayer.clearStuff();
						practicePlayer.clearAllEffects();
						practicePlayer.joinInSpawn();

						practicePlayer.setPlayerStatus(PlayerStatus.INSPAWN);
						practicePlayer.setFight(null);

						player.teleport(PracticeManager.getInstance().getSpawnLocation());
						player.setFireTicks(0);

						EnderPearlsPatch.timers.remove(player);
						player.setLevel(0);

						stop(player);
						return;
					}
				}
			} else {
				// AUTRE JEUX QUE LE RUSH
				if (!(isFightTeam())) {
					PracticeManager.getInstance().getQueue(practicePlayer).getPlayersInFight().remove(practicePlayer);

					List<PotionEffect> loserPotions = new ArrayList<>();
					for (PotionEffect potions : player.getActivePotionEffects()) {
						if (potions != null) {
							loserPotions.add(potions);
						}
					}
					new InventoryDuel(loserPotions, (int) ((Damageable) player).getHealth(), player.getFoodLevel(),
							player);

					practicePlayer.clearStuff();
					practicePlayer.clearAllEffects();
					practicePlayer.joinInSpawn();

					practicePlayer.setPlayerStatus(PlayerStatus.INSPAWN);
					practicePlayer.setFight(null);

					player.teleport(PracticeManager.getInstance().getSpawnLocation());
					player.setFireTicks(0);

					EnderPearlsPatch.timers.remove(player);
					player.setLevel(0);

					stop(player);
				} else {
				}
			}
		} else {
			sendMessage(PracticeManager.PREFIX + "�f" + player.getName() + " �fest mort en d�connectant !");
			if (!(isFightTeam())) {
				stop(player);
			} else {

			}
		}
	}

	public List<PracticePlayer> getPlayers() {
		List<PracticePlayer> players = new ArrayList<>();
		for (String oppenent : this.oppenents) {
			PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(oppenent);
			if (practicePlayer != null) {
				players.add(practicePlayer);
			}
		}
		for (String player : this.players) {
			PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player);
			if (practicePlayer != null) {
				players.add(practicePlayer);
			}
		}
		return players;
	}

	public List<PracticePlayer> getPlayersInFight() {
		List<PracticePlayer> players = new ArrayList<>();
		for (String oppenent : this.oppenents) {
			PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(oppenent);
			if (practicePlayer != null) {
				players.add(practicePlayer);
			}
		}
		for (String player : this.players) {
			PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player);
			if (practicePlayer != null) {
				players.add(practicePlayer);
			}
		}
		return players;
	}

	public void sendMessage(String message) {
		for (PracticePlayer players : getPlayers())
			if (players.isOnline())
				players.getPlayer().sendMessage(message);
	}

	public GameType getGameType() {
		return this.gameType;
	}

	public FightType getFightType() {
		return this.fightType;
	}

	public ArenaAbstract getArena() {
		return this.arena;
	}

	public long getTimeElapsed() {
		return System.currentTimeMillis() - this.startGame;
	}

	public boolean gameIsFinish() {
		return this.gameFinished;
	}

	public String getOppenent(Player player) {
		if (this.players.contains(player.getName()))
			return this.oppenents.get(0);
		if (this.oppenents.contains(player.getName()))
			return this.players.get(0);
		return null;
	}

	public String getPlayer(Player player) {
		if (this.players.contains(player.getName()))
			return this.players.get(0);
		if (this.oppenents.contains(player.getName()))
			return this.oppenents.get(0);
		return null;
	}

	public boolean isFightTeam() {
		return (fightType != FightType.UNRANKED && fightType != FightType.RANKED && fightType != FightType.DUEL);
	}

	public List<String> getPlayersList() {
		return this.players;
	}

	public List<String> getOppenentsList() {
		return this.oppenents;
	}

	public List<String> getOppenents(Player player) {
		if (this.players.contains(player.getName()))
			return this.oppenents;
		if (this.oppenents.contains(player.getName()))
			return this.players;
		return null;
	}

	public List<String> getAllies(Player player) {
		if (this.players.contains(player.getName()))
			return this.players;
		if (this.oppenents.contains(player.getName()))
			return this.oppenents;
		return null;
	}
}
