package mc.bloostrynetwork.fr.bloostrypractice.utils.items;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Item {
	
	private ItemStack item;
	private ItemMeta itemMeta;
	private ItemHandler itemHandler;

	public Item(Material material, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material, 1);
		this.itemMeta = item.getItemMeta();
		if(name != null)
		this.itemMeta.setDisplayName(name);
		if(lore != null)
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	} 

	public Item(Material material, byte durability, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material);
		this.item.setDurability(durability);
		this.itemMeta = item.getItemMeta();
		if(name != null)
		this.itemMeta.setDisplayName(name);
		if(lore != null)
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}
	
	/* --- --- --- ---*/

	public Item(Material material, int amount, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material, amount);
		this.itemMeta = item.getItemMeta();
		this.itemMeta.setDisplayName(name);
		if(lore != null)
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}

	public Item(Material material, int amount, byte durability, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material, amount);
		this.item.setDurability(durability);
		this.itemMeta = item.getItemMeta();
		if(name != null)
		this.itemMeta.setDisplayName(name);
		if(lore != null)
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}
	
	public void setName(String name){
		this.itemMeta.setDisplayName(name);
		this.item.setItemMeta(itemMeta);
	}
	
	public String getName(){
		return this.itemMeta.getDisplayName();
	}
	
	public void setLore(List<String> lore){
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
	}
	
	public List<String> getLore(){
		return this.itemMeta.getLore();
	}

	public Map<Enchantment, Integer> getEnchantements(){
		return this.itemMeta.getEnchants();
	}
	
	public void addEnchantment(Enchantment enchantment, Integer level, Boolean result){
		this.itemMeta.addEnchant(enchantment, level, result);
		this.item.setItemMeta(itemMeta);
	}
	
	public void removeEnchantment(Enchantment enchantment){
		this.itemMeta.removeEnchant(enchantment);
		this.item.setItemMeta(itemMeta);
	}
	
	public Boolean hasEnchantment(Enchantment enchantment){
		return this.hasEnchantment(enchantment);
	}
	
	public ItemStack toItemStack(){
		return this.item;
	}

	public void setDurability(short s) {
		this.item.setDurability(s);
		this.item.setItemMeta(itemMeta);
	}

	public ItemHandler getItemHandler() {
		return itemHandler;
	}

	public void setItemHandler(ItemHandler itemHandler) {
		this.itemHandler = itemHandler;
	}

	public void handle(Player player) {
		if(itemHandler == null)return;
		itemHandler.handle(player);
	}
}
