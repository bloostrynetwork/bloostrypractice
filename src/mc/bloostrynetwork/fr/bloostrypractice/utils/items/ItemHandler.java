package mc.bloostrynetwork.fr.bloostrypractice.utils.items;

import org.bukkit.entity.Player;

public interface ItemHandler {

	public abstract void handle(Player player);
	
}
