package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerItemDropListener implements Listener {

	@EventHandler
	public void PlayerDropItemEvent_(PlayerDropItemEvent e) {
		Player player = e.getPlayer();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		if (e.getItemDrop() != null) {
			if (practicePlayer.getPlayerStatus().isInvinsible()) {
				if (e.getItemDrop().getItemStack() != null && e.getItemDrop().getItemStack().getItemMeta() != null
						&& e.getItemDrop().getItemStack().getItemMeta().hasDisplayName()) {
					for (Item item : practicePlayer.items.values()) {
						if (item.getName()
								.equalsIgnoreCase(e.getItemDrop().getItemStack().getItemMeta().getDisplayName())) {
							e.setCancelled(true);
						}
					}
				}
			}
		}
		if (practicePlayer.getFight() != null) {
			Bukkit.getScheduler().runTaskLater(BloostryPractice.getInstance(), new Runnable() {
				@Override
				public void run() {
					org.bukkit.entity.Item item = e.getItemDrop();
					if (item != null)
						item.remove();
				}
			}, 20 * 2);
		} else if (practicePlayer.getPlayerStatus() == PlayerStatus.INEDITKIT) {
			e.setCancelled(true);
		}
	}
}
