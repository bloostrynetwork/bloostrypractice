package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import mc.bloostrynetwork.fr.bloostrypractice.commands.ArenaICommand;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class BlockListener implements Listener {

	@EventHandler
	public void BlockBreakEvent_(BlockBreakEvent e) {
		Player player = (Player) e.getPlayer();

		if (ArenaICommand.playersInCreateMode.contains(player.getName())) {
			if (e.getBlock().getType() == Material.BED_BLOCK || e.getBlock().getType() == Material.BED) {
				ArenaICommand.bedLoc2.put(player.getName(), e.getBlock().getLocation());
				player.sendMessage(PracticeManager.PREFIX + "�6Clic-gauche sur le lit �7(�e"
						+ e.getBlock().getLocation().getBlockX() + "�6,�e" + e.getBlock().getLocation().getBlockY()
						+ "�6,�e" + e.getBlock().getLocation().getBlockZ() + "�7)");
				e.setCancelled(true);
			} else {
				ArenaICommand.leftClick.put(player.getName(), e.getBlock().getLocation());
				player.sendMessage(PracticeManager.PREFIX + "�6Clic-gauche sur le bloc �7(�e"
						+ e.getBlock().getLocation().getBlockX() + "�6,�e" + e.getBlock().getLocation().getBlockY()
						+ "�6,�e" + e.getBlock().getLocation().getBlockZ() + "�7)");
				e.setCancelled(true);
			}
		}

		if (player.getGameMode() == GameMode.CREATIVE)
			return;

		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());

		if (practicePlayer.getPlayerStatus().isInvinsible()) {
			e.setCancelled(true);
		} else {
			if (practicePlayer.getPlayerStatus() == PlayerStatus.INFIGHT) {
				if (practicePlayer.getFight().getGameType() == GameType.BUILDUHC) {
					if (e.getBlock().getType() == Material.COBBLESTONE || e.getBlock().getType() == Material.WOOD) {
						if(!(practicePlayer.getFight().getArena().getCuboide().isInCube(e.getBlock().getLocation()))) {
							player.sendMessage(PracticeManager.PREFIX + "�cVous ne pouvez casser de bloquer en dehors de la map");
							e.setCancelled(true);
						}
						return;
					}
				}
				if (practicePlayer.getFight().getGameType() == GameType.RUSH) {
					if (e.getBlock().getType() == Material.SANDSTONE || e.getBlock().getType() == Material.TNT) {
						if(!(practicePlayer.getFight().getArena().getCuboide().isInCube(e.getBlock().getLocation()))) {
							player.sendMessage(PracticeManager.PREFIX + "�cVous ne pouvez casser de bloquer en dehors de la map");
							e.setCancelled(true);
						}
						return;
					}
				}
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void BlockPlaceEvent_(BlockPlaceEvent e) {
		Player player = (Player) e.getPlayer();

		if (player.getGameMode() == GameMode.CREATIVE)
			return;

		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());

		if (practicePlayer.getPlayerStatus().isInvinsible()) {
			e.setCancelled(true);
		} else {
			if (practicePlayer.getPlayerStatus() == PlayerStatus.INFIGHT) {
				if (practicePlayer.getFight().getGameType() == GameType.BUILDUHC) {
					if (e.getBlock().getType() == Material.COBBLESTONE || e.getBlock().getType() == Material.WOOD) {
						if(!(practicePlayer.getFight().getArena().getCuboide().isInCube(e.getBlock().getLocation()))) {
							player.sendMessage(PracticeManager.PREFIX + "�cVous ne pouvez placer de bloquer en dehors de la map");
							e.setCancelled(true);
						}
						return;
					}
				}
				if (practicePlayer.getFight().getGameType() == GameType.RUSH) {
					if (e.getBlock().getType() == Material.SANDSTONE || e.getBlock().getType() == Material.TNT) {
						if(!(practicePlayer.getFight().getArena().getCuboide().isInCube(e.getBlock().getLocation()))) {
							player.sendMessage(PracticeManager.PREFIX + "�cVous ne pouvez placer de bloquer en dehors de la map");
							e.setCancelled(true);
						}
						return;
					}
				}
				e.setCancelled(true);
			}
		}
	}
}
