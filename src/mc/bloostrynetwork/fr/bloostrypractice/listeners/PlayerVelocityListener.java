package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerVelocityEvent;

public class PlayerVelocityListener implements Listener {

	// CODE FOR CANCELLED NATURAL KNOKCNACK MINECRAFT
	@EventHandler
	public void onPlayerVelocity(PlayerVelocityEvent e) {
		Player p = e.getPlayer();
		EntityDamageEvent lastDamageCause = p.getLastDamageCause();
		if ((lastDamageCause == null) || (!(lastDamageCause instanceof EntityDamageByEntityEvent))) {
			return;
		}
		if ((((EntityDamageByEntityEvent) lastDamageCause).getDamager() instanceof Player)) {
			e.setCancelled(true);
		}
	}
}