package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.util.Vector;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaRush;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class EntityExplodeListener implements Listener {

	@EventHandler
	public void EntityExplodeEvent_(EntityExplodeEvent e) {
		Entity entity = e.getEntity();
		
		if (entity instanceof TNTPrimed) {
			TNTPrimed tntprimedEntity = (TNTPrimed) entity;

			Entity author = tntprimedEntity.getSource();

			if (author instanceof Player) {
				Player authorPlayer = ((Player) author);
				PracticePlayer practicePlayerAuthor = PracticeManager.getInstance()
						.getPracticePlayer(authorPlayer.getName());

				Fight fight = practicePlayerAuthor.getFight();

				ArenaRush arenaRush = (ArenaRush) fight.getArena();
				
				if (fight != null && fight.getGameType() == GameType.RUSH) {					
					for (Block block : e.blockList()) {
						if (block.getType() == Material.TNT) {
							Location location = block.getLocation().add(0.5, 0.25, 0.5);
							TNTPrimed tnt = (TNTPrimed) block.getWorld().spawnEntity(location, EntityType.PRIMED_TNT);
							tnt.setVelocity(new Vector(0, 0.25, 0));
							tnt.teleport(location);
						} else if (block.getType() == Material.SANDSTONE) {
							block.setType(Material.AIR);
						} else if (block.getType() == Material.BED || block.getType() == Material.BED_BLOCK) {
							block.setType(Material.AIR);
							
							if(arenaRush.getBedLocation1().distance(block.getLocation()) < 4) {
								if(fight.isFightTeam()) {
									fight.sendMessage(PracticeManager.PREFIX + "�fLe lit de l'�quipe de �e"+fight.getPlayersList().get(0)+" �fvient d'�tre d�truit !");
								} else {
									fight.sendMessage(PracticeManager.PREFIX + "�fLe lit du joueur �e"+fight.getPlayersList().get(0)+" �fvient d'�tre d�truit !");	
								}
							}
							
							if(arenaRush.getBedLocation2().distance(block.getLocation()) < 4) {
								if(fight.isFightTeam()) {
									fight.sendMessage(PracticeManager.PREFIX + "�fLe lit de l'�quipe de �e"+fight.getOppenentsList().get(0)+" �fvient d'�tre d�truit !");
								} else {
									fight.sendMessage(PracticeManager.PREFIX + "�fLe lit du joueur �e"+fight.getOppenentsList().get(0)+" �fvient d'�tre d�truit !");	
								}
							}
						} else {
							e.blockList().remove(block);
						}
					}
				}
			}
		}
	}
}
