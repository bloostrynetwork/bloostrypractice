package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class EntityRegainHealthListener implements Listener {

	@EventHandler
	public void EntityRegainHealthEvent_(EntityRegainHealthEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
			Fight fight = practicePlayer.getFight();
			if (e.getRegainReason() == RegainReason.EATING) {
				e.setCancelled(true);
			}
		}
	}
}
