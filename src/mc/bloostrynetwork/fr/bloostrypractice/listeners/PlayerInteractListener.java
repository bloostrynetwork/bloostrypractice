package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.fr.bloostrypractice.commands.ArenaICommand;
import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiSaveKit;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerInteractListener implements Listener {
	@EventHandler
	public void PlayerInterractEvent_(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		if (e.getAction().toString().contains("RIGHT")) {
			ItemStack item = player.getItemInHand();
			if ((ArenaICommand.playersInCreateMode.contains(player.getName())) && (e.getClickedBlock() != null)) {
				if (e.getClickedBlock().getType() == Material.BED_BLOCK || e.getClickedBlock().getType() == Material.BED) {
					ArenaICommand.bedLoc1.put(player.getName(), e.getClickedBlock().getLocation());
					player.sendMessage(PracticeManager.PREFIX + "�6Clic-droit sur le lit �7(�e"
							+ e.getClickedBlock().getLocation().getBlockX() + "�6,�e"
							+ e.getClickedBlock().getLocation().getBlockY() + "�6,�e"
							+ e.getClickedBlock().getLocation().getBlockZ() + "�7)");
				} else {
					ArenaICommand.rightClick.put(player.getName(), e.getClickedBlock().getLocation());
					player.sendMessage(PracticeManager.PREFIX + "�6Clic-droit sur le bloc �7(�e"
							+ e.getClickedBlock().getLocation().getBlockX() + "�6,�e"
							+ e.getClickedBlock().getLocation().getBlockY() + "�6,�e"
							+ e.getClickedBlock().getLocation().getBlockZ() + "�7)");
				}
				return;
			}
			if (item != null) {
				if ((item.getItemMeta() != null) && (item.getItemMeta().hasDisplayName())) {
					for (Item items : practicePlayer.items.values()) {
						if (items.getName().equalsIgnoreCase(item.getItemMeta().getDisplayName())) {
							items.handle(player);
							return;
						}
					}
					if (item.getItemMeta().getDisplayName().equalsIgnoreCase("�cKit D�faut")) {
						player.sendMessage(PracticeManager.PREFIX + " �fVous avez s�lectioner votre kit d�faut.");
						Fight fight = practicePlayer.getFight();
						if (fight != null) {
							fight.getGameType().getIKit().giveKitDefault(player);
						}
						return;
					}
					if (item.getItemMeta().getDisplayName().equalsIgnoreCase("�cKit Modifi�")) {
						if (practicePlayer.getFight().getGameType().getIKit().hasKitEdited(player)) {
							practicePlayer.getFight().getGameType().getIKit().giveKitEdited(player);
							player.sendMessage(PracticeManager.PREFIX + " �fVous avez s�lectionner votre kit modifi�.");
							Fight fight = practicePlayer.getFight();
							if (fight != null) {
								fight.getGameType().getIKit().giveKitEdited(player);
							}
						} else {
							player.sendMessage(PracticeManager.PREFIX + " �cVous n'avez pas de kit modifi�.");
						}
						return;
					}
				}
				if (practicePlayer.getPlayerStatus().isInvinsible()) {
					if (player.getGameMode() == GameMode.CREATIVE) {
						return;
					}
					player.getInventory().remove(item);
					e.setCancelled(true);
				}
			}
		}
		if ((e.getAction().toString().contains("BLOCK")) && (practicePlayer.getPlayerStatus() == PlayerStatus.INEDITKIT)
				&& (e.getClickedBlock().getType().equals(Material.ANVIL))) {
			e.setCancelled(true);
			new GuiSaveKit().open(player);
		}
	}
}
