package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.queue.Queue;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerConnectionListener implements Listener {

	@EventHandler
	public void PlayerJoinEvent_(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		e.setJoinMessage(null);
		PlayerJoin(player);
	}

	@EventHandler
	public void PlayerQuitEvent_(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		e.setQuitMessage(null);
		//BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		
		Console.sendMessageDebug("practicePlayer -> "+practicePlayer);
		
		Fight fight = practicePlayer.getFight();
		if(fight != null) {
			fight.death("deconnection", player);
		}
		
		Queue queue = PracticeManager.getInstance().getQueue(practicePlayer);
		
		if(queue != null) {
			if(queue.getPlayersInQueue().contains(practicePlayer)) {
				queue.getPlayersInQueue().remove(practicePlayer);
			}
			
			if(queue.getPlayersInFight().contains(practicePlayer)) {
				PracticeManager.getInstance().getQueue(practicePlayer).getPlayersInFight().remove(practicePlayer);
			}	
		}
				
		PracticeManager.getInstance().removePracticePlayer(player);
	}

	public static void PlayerJoin(Player player) {
		BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());
		if (bloostryPlayer.getBloostryRank().getPermissionPower() >= Ranks.VIP.getBloostryRank().getPermissionPower()) {
			Bukkit.broadcastMessage(bloostryPlayer.getBloostryRank().getPrefix() + bloostryPlayer.getName()
					+ " �e� rejoint le serveur.");
		}
		PracticeManager.getInstance().addPracticePlayer(player);
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		practicePlayer.joinInSpawn();
		practicePlayer.teleportToSpawn();
		player.sendMessage("�8�m"+PracticeManager.LINE_SPACER);
		player.sendMessage("�8");
		player.sendMessage(PracticeManager.PREFIX+"�7Le serveur est actuellement en �cALPHA");
		player.sendMessage("�7donc si vous remarquez un bug veuillez nous en faire par");
		player.sendMessage("�7avec la commande: �f/reportbug �e<message>");
		player.sendMessage("�8");
		player.sendMessage("�7Merci de votre compr�hension et bon jeux ");
		player.sendMessage("�7sur notre serveur :D");
		player.sendMessage("�8");
		player.sendMessage("�8�m"+PracticeManager.LINE_SPACER);
	}
}
