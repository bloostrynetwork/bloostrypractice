package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerItemConsumeListener implements Listener {
	
	@EventHandler
	public void PlayerItemConsumeEvent_(PlayerItemConsumeEvent e) {
		Player player = e.getPlayer();
		if ((e.getItem() != null) && (e.getItem().hasItemMeta()) && (e.getItem().getItemMeta().hasDisplayName())
				&& (e.getItem().getItemMeta().getDisplayName().equals("�6Golden Head"))) {
			PotionEffectType type = PotionEffectType.ABSORPTION;
			PotionEffect pEffect = new PotionEffect(type, 2400, 1);
			PotionEffectType type2 = PotionEffectType.REGENERATION;
			PotionEffect p2Effect = new PotionEffect(type2, 160, 1);

			player.addPotionEffect(pEffect);
			player.addPotionEffect(p2Effect);
		}
	}
}
