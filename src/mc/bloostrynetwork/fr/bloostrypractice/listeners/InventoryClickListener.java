package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiEditKit;
import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiRanked;
import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiSaveKit;
import mc.bloostrynetwork.fr.bloostrypractice.gui.spawn.GuiUnranked;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.IGui;

public class InventoryClickListener implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void InventoryClickEvent_(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		
		if(inv.getTitle().contains("§cInventaire")) {
			e.setCancelled(true);
			return;
		}
		
		Player player = (Player) e.getWhoClicked();

		List<IGui> guis = new ArrayList<>();
		guis.add(new GuiRanked());
		guis.add(new GuiUnranked());
		guis.add(new GuiEditKit());
		guis.add(new GuiSaveKit());

		for (IGui gui : guis) {
			if (inv.getName().equals(gui.getGUI().getTitle())) {
				e.setCancelled(true);
				ItemStack itemStack = e.getCurrentItem();
				if (itemStack != null && itemStack.hasItemMeta()) {
					gui.clickOnItem(player, e.getSlot());
				}
			}
		}
	}
}
