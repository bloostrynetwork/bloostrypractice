package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;
import net.minecraft.server.v1_7_R4.PacketPlayOutEntityVelocity;

public class EntityDamageListener implements Listener {

	@EventHandler
	public void EntityDamageEvent_(EntityDamageEvent e) {
		if (!(e.getEntity() instanceof Player))
			return;
		Player player = (Player) e.getEntity();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());

		if (practicePlayer.getPlayerStatus().isInvinsible()) {
			e.setCancelled(true);
			return;
		}

		Fight fight = practicePlayer.getFight();
		if (fight != null) {
			if (fight.gameIsFinish()) {
				e.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void EntityDamageByEntityEvent_(EntityDamageByEntityEvent e) {
		if (!(e.getEntity() instanceof Player))
			return;
		Player player = (Player) e.getEntity();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());

		if (practicePlayer.getPlayerStatus().isInvinsible()) {
			e.setCancelled(true);
			return;
		}

		Fight fight = practicePlayer.getFight();
		if (fight != null) {
			if (fight.gameIsFinish()) {
				e.setCancelled(true);
				return;
			}
		}

		if ((e.getDamager() instanceof Player)) {
			if ((e.getEntity() instanceof Player)) {

				Player damaged = (Player) e.getEntity();
				Player damager = (Player) e.getDamager();
				
				PracticePlayer damagerPractice = PracticeManager.getInstance().getPracticePlayer(damager.getName());

				fight = damagerPractice.getFight();
				
				String gameType = fight.getGameType().getName().replace(GameType.NODEBUFF.getName(), "potion").replace(GameType.DEBUFF.getName(), "potion").replace(GameType.BUILDUHC.getName(), "builduhc");
				
				if (damaged.getNoDamageTicks() > damaged.getMaximumNoDamageTicks() / 2D) {
					return;
				}

				double horMultiplier = damager.isSprinting()
						? BloostryPractice.getInstance().getConfig().getDouble("knockback-configuration."+gameType+".sprint.hor")
						: BloostryPractice.getInstance().getConfig().getDouble("knockback-configuration."+gameType+".no-sprint.hor");
						
				double verMultiplier = damager.isSprinting()
						? BloostryPractice.getInstance().getConfig().getDouble("knockback-configuration."+gameType+".sprint.ver")
						: BloostryPractice.getInstance().getConfig().getDouble("knockback-configuration."+gameType+".no-sprint.ver");
						
				double kbMultiplier = damager.getItemInHand() == null ? 0
						: damager.getItemInHand().getEnchantmentLevel(Enchantment.KNOCKBACK) * 0.2D;
				
				@SuppressWarnings("deprecation")
				double airMultiplier = damaged.isOnGround() ? 1.0 : 1.25D;

				Vector knockback = damaged.getLocation().toVector().subtract(damager.getLocation().toVector())
						.normalize();
				knockback.setX(((knockback.getX() /* * sprintMultiplier */ + kbMultiplier) * horMultiplier));
				knockback.setY(0.35D * airMultiplier * verMultiplier);
				knockback.setZ(((knockback.getZ() /* * sprintMultiplier */ + kbMultiplier) * horMultiplier));

				sendKnockback(damaged, damaged.getEntityId(), /*(damaged.isSprinting() ? knockback.getX() * 2 : knockback.getX())*/ knockback.getX(), knockback.getY(),  /* (damaged.isSprinting() ? knockback.getZ() * 2 : knockback.getZ())*/ knockback.getZ());
				return;
			}
		}

		if ((e.getDamager() instanceof Arrow)) {
			Arrow a1 = (Arrow) e.getDamager();
			if ((a1.getShooter() instanceof Player)) {
				Player shooter = (Player) a1.getShooter();

				Damageable dp = (Damageable) e.getEntity();
				if ((dp instanceof Player)) {
					Player victim = (Player) dp;
					double vh = dp.getHealth();
					Integer damage = Integer.valueOf((int) e.getFinalDamage());
					if (!dp.isDead()) {
						Integer health = Integer.valueOf((int) (vh - damage.intValue()));
						if (health.intValue() > 0) {
							// this.pr.replace('&', '�') + "�a" + victim.getName() + "�6 is now at �a" +
							// health + " �4" + "?"
							shooter.sendMessage(PracticeManager.PREFIX+" §e"+victim.getName()+" §f>> §c"+health+" §c"+'❤');
						}
					}
				}
			}
		}
	}

	public void sendKnockback(Entity p, int id, double x, double y, double z) {
		PacketPlayOutEntityVelocity packet = new PacketPlayOutEntityVelocity(id, x, y, z);

		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}
}
