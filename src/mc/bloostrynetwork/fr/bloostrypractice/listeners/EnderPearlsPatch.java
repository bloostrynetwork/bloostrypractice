package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class EnderPearlsPatch implements Listener {

	public static HashMap<Player, Integer> timers = new HashMap<>();

	@EventHandler
	public void PlayerInterractEvent_(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		
		if (practicePlayer == null) {
			return;
		}
		
		if (e.getAction().toString().contains("RIGHT")) {
			ItemStack itemStack = e.getItem();
			if ((itemStack != null) && (itemStack.getType().equals(Material.ENDER_PEARL)) && practicePlayer.getPlayerStatus().isInvinsible()) {
				e.setCancelled(true);
				return;
			}
		}
		if ((e.getAction() == Action.LEFT_CLICK_AIR) || (e.getAction() == Action.LEFT_CLICK_BLOCK)
				|| (e.getItem() == null) || (e.getItem().getType() != Material.ENDER_PEARL)) {
			return;
		}
		
		if ((e.getClickedBlock() != null) && (!e.isCancelled()) && (!e.getPlayer().isSneaking())) {
			Material clickedMat = e.getClickedBlock().getType();
			if (interactables.contains(clickedMat)) {
				return;
			}
		}
		if ((player.getGameMode() == GameMode.SURVIVAL) || (player.getGameMode() == GameMode.ADVENTURE)) {
			if (!timers.containsKey(player)) {
				timers.put(player, Integer.valueOf(16));
				player.setLevel(16);
			} else {
				e.setCancelled(true);
			}
		}
	}

	public static void repeatTimer() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(BloostryPractice.getInstance(), new Runnable() {
			public void run() {
				if (EnderPearlsPatch.timers.size() != 0) {
					for (Player pls : EnderPearlsPatch.timers.keySet()) {
						if (((Integer) EnderPearlsPatch.timers.get(pls)).intValue() != 0) {
							EnderPearlsPatch.timers.put(pls,
									Integer.valueOf(((Integer) EnderPearlsPatch.timers.get(pls)).intValue() - 1));
							pls.setLevel(((Integer) EnderPearlsPatch.timers.get(pls)).intValue());
						} else {
							EnderPearlsPatch.timers.remove(pls);
							pls.setLevel(0);
						}
					}
				}
			}
		}, 20L, 20L);
	}

	private static final Set<Material> interactables = new HashSet<>(Arrays.asList(new Material[] { Material.ANVIL,
			Material.COMMAND, Material.BED, Material.BEACON, Material.BED_BLOCK, Material.BREWING_STAND,
			Material.BURNING_FURNACE, Material.CAKE_BLOCK, Material.CHEST, Material.DIODE, Material.DIODE_BLOCK_OFF,
			Material.DIODE_BLOCK_ON, Material.DISPENSER, Material.DROPPER, Material.ENCHANTMENT_TABLE,
			Material.ENDER_CHEST, Material.FENCE_GATE, Material.FENCE_GATE, Material.FURNACE, Material.HOPPER,
			Material.IRON_DOOR, Material.IRON_DOOR_BLOCK, Material.ITEM_FRAME, Material.LEVER,
			Material.REDSTONE_COMPARATOR, Material.REDSTONE_COMPARATOR_OFF, Material.REDSTONE_COMPARATOR_ON,
			Material.STONE_BUTTON, Material.TRAP_DOOR, Material.TRAPPED_CHEST, Material.WOODEN_DOOR,
			Material.WOOD_BUTTON, Material.WOOD_DOOR, Material.WORKBENCH }));
}
