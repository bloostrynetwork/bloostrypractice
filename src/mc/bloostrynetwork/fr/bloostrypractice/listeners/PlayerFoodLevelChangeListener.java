package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerFoodLevelChangeListener implements Listener {


	@EventHandler
	public void PlayerFoodLevelChangeEvent_(FoodLevelChangeEvent e) {
		if(!(e.getEntity() instanceof Player))return;
		Player player = (Player) e.getEntity();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		
		if(practicePlayer.getPlayerStatus().isInvinsible()) {
			e.setCancelled(true);
		}
	}
}
