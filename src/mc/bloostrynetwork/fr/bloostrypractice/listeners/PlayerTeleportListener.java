package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerTeleportListener implements Listener {

	@EventHandler
	public void PlayerTeleportEvent_(PlayerTeleportEvent e) {
		if (e.getCause() == TeleportCause.ENDER_PEARL) {
			Player player = e.getPlayer();
			PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());

			if (practicePlayer == null) {
				return;
			}

			if (practicePlayer.getPlayerStatus().isInvinsible()) {
				e.setCancelled(true);
			}

			String emptyblock = "0,6,8,9,10,11,30,31,32,37,38,39,40,50,51,55,59,63,65,66,68,69,70,72,75,76,77,83,90,93,94,104,105,106,115";

			if (practicePlayer.getPlayerStatus() == PlayerStatus.INFIGHT && practicePlayer.getFight() != null) {
				Location loc = e.getTo();
				loc.setX(loc.getBlockX() + 0.5D);
				loc.setY(loc.getBlockY());
				loc.setZ(loc.getBlockZ() + 0.5D);
				if ((!isIdInList(loc.getBlock().getTypeId(), emptyblock))
						&& (!isIdInList(loc.getBlock().getRelative(BlockFace.UP).getTypeId(), emptyblock))) {
					loc = e.getFrom();
				}
			}
		}
	}

	protected boolean isIdInList(int id, String str) {
		String[] ln = str.split(",");
		if (ln.length > 0) {
			for (int i = 0; i < ln.length; i++) {
				if (Integer.parseInt(ln[i]) == id) {
					return true;
				}
			}
		}
		return false;
	}
}
