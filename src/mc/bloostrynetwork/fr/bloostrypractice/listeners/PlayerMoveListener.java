package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerMoveListener implements Listener {

	@EventHandler
	public void PlayerMoveEvent_(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
		
		if(practicePlayer.getPlayerStatus().isInvinsible()) {
			if(e.getTo().getBlockY() <= 20) {
				practicePlayer.teleportToSpawn();
			}
		} else {
			Fight fight = practicePlayer.getFight();
			if(fight.getGameType() == GameType.RUSH) {
				if(e.getTo().getBlockY() <= 20) {
					fight.death("void", player);
				}
			}
		}
	}
}
