package mc.bloostrynetwork.fr.bloostrypractice.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerDeathListener implements Listener {

	@EventHandler
	public void PlayerDeathEvent_(PlayerDeathEvent e) {
		e.setDeathMessage(null);
		e.setKeepInventory(true);
		e.setKeepLevel(true);
		Player player = e.getEntity();
		PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());

		Fight fight = practicePlayer.getFight();
		if(fight != null) {
			fight.death("damage", player);
		}
	}
}
