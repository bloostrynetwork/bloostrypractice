package mc.bloostrynetwork.fr.bloostrypractice;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;

public class pingICommand implements ICommand {

	@Override
	public String getCommand() {
		return "ping";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if(args.length == 0) {
			if(commandSender instanceof Player) {
				Player player = (Player) commandSender;
				commandSender.sendMessage(PracticeManager.PREFIX  + "�fVotre latence est de �e"+((CraftPlayer) player).getHandle().ping+" �fms�f.");	
			}
		} else if (args.length == 1) {
			Player target = (Player) Bukkit.getPlayer(args[0]);
			if(target != null) {
				commandSender.sendMessage(PracticeManager.PREFIX  + "�eLa latence de �e"+target.getName()+" �Fest de �e"+((CraftPlayer) target).getHandle().ping+" �fms�f.");	
			} else {
				commandSender.sendMessage(PracticeManager.PREFIX + "�cCe joueur n'est pas en ligne !");
			}
		}
	}
}
