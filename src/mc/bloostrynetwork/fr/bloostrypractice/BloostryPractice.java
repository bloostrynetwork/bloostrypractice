package mc.bloostrynetwork.fr.bloostrypractice;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.commands.ArenaICommand;
import mc.bloostrynetwork.fr.bloostrypractice.commands.ReportBugICommand;
import mc.bloostrynetwork.fr.bloostrypractice.commands.inventoryICommand;
import mc.bloostrynetwork.fr.bloostrypractice.commands.knockbackICommand;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerConnectionListener;
import mc.bloostrynetwork.fr.bloostrypractice.manager.EventsManager;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.runnable.PlayerManagerRunnable;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaAbstract;

public class BloostryPractice extends JavaPlugin {

	private static BloostryPractice instance;

	public static File FOLDER_CORE;
	
	public static File STATS_FOLDER;
	public static File ARENA_FOLDER;
	public static File KITS_FOLDER;
	
	@Override
	public void onEnable() {
		instance = this;
		
		if(!(Bukkit.getPluginManager().isPluginEnabled(Main.javaPlugin.getDescription().getName())) || (!(Main.bloostryAPI.isEnabled()))) {
			for(Player players : Bukkit.getOnlinePlayers()) {
				players.sendMessage("�8�m-----------------------------------------------------");
				players.sendMessage("");
				players.sendMessage("�cErreur fatal, le plugin n'est pas utilisable car l'api ");
				players.sendMessage("�cne fonctionne pas correctement !");
				players.sendMessage("");
				players.sendMessage("�7Information:");
				players.sendMessage("  �eAuteur: �fDarkTwister_ (Antox11200)");
				players.sendMessage("  �eVersion: �f" + getDescription().getVersion());
				players.sendMessage("  �eDistribu� pour: �fBloostryNetwork");
				players.sendMessage("");
				players.sendMessage("�8�m-----------------------------------------------------");
			}
			
			Console.sendMessageInfo("�8�m-----------------------------------------------------");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�cErreur fatal, le plugin n'est pas utilisable car l'api ");
			Console.sendMessageInfo("�cne fonctionne pas correctement !");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�7Information:");
			Console.sendMessageInfo("  �eAuteur: �fDarkTwister_ (Antox11200)");
			Console.sendMessageInfo("  �eVersion: �f" + getDescription().getVersion());
			Console.sendMessageInfo("  �eDistribu� pour: �fBloostryNetwork");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�cSi le plugin est sur son serveur d'origine veuillez");
			Console.sendMessageInfo("�ccontacter le d�veloppeur !");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�8�m-----------------------------------------------------");
			return;
		}
		
		getConfig().options().copyDefaults(true);
		saveConfig();

		FOLDER_CORE = new File("plugins/BloostryPractice/");
		FOLDER_CORE.mkdir();

		STATS_FOLDER = new File(FOLDER_CORE.getAbsolutePath()+"/stats/");
		STATS_FOLDER.mkdir();

		ARENA_FOLDER = new File(FOLDER_CORE.getAbsolutePath()+"/arena/");
		ARENA_FOLDER.mkdir();

		KITS_FOLDER = new File(FOLDER_CORE.getAbsolutePath()+"/kits/");
		KITS_FOLDER.mkdir();

		Console.sendMessageInfo("Create folder for stats,kits and for arena");
		
		new EventsManager().registerListeners();
		Console.sendMessageInfo("Register all listener for events");

		Main.bloostryAPI.getCommandsManager().addCommand(this, new ReportBugICommand());
		Main.bloostryAPI.getCommandsManager().addCommand(this, new ArenaICommand());
		Main.bloostryAPI.getCommandsManager().addCommand(this, new knockbackICommand());
		Main.bloostryAPI.getCommandsManager().addCommand(this, new pingICommand());
		Main.bloostryAPI.getCommandsManager().addCommand(this, new inventoryICommand());

		Main.bloostryAPI.getCommandsManager().registerCommands();
		
		Console.sendMessageInfo("Register all commands");
		
		PracticeManager practiceManager = new PracticeManager();
		practiceManager.initArena();

		String headLine = "�8-----------------�7[ �cPractice �7]�8-----------------";
		String endLine = "�8----------------------------------------------";

		Console.sendMessageInfo(headLine);
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�eMain: �6" + getDescription().getMain());
		Console.sendMessageInfo("�eAuthor: �6" + getDescription().getName());
		Console.sendMessageInfo("�eVersion: �6" + getDescription().getVersion());
		Console.sendMessageInfo("�eCommands: �6");
		for (String commands : getDescription().getCommands().keySet()) {
			Console.sendMessageInfo("�6- �e/" + commands);
		}
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�aPlugin enabled");
		Console.sendMessageInfo("");
		Console.sendMessageInfo(endLine);
		
		Bukkit.broadcastMessage("�8�m"+PracticeManager.LINE_SPACER);
		Bukkit.broadcastMessage("");
		Bukkit.broadcastMessage("�7Le plugin vient d'�tre rafraichie, d�soler de la g�ne occasionn�e");
		Bukkit.broadcastMessage("");
		Bukkit.broadcastMessage(PracticeManager.LINE_SPACER);
		
		for(Player players : Bukkit.getOnlinePlayers()) {
			PlayerConnectionListener.PlayerJoin(players);
		}
		
		new PlayerManagerRunnable();
	}
	
	@Override
	public void onDisable() {
		for(ArrayList<ArenaAbstract> arenas : PracticeManager.getInstance().arenas.values()) {
			for(ArenaAbstract arena : arenas) {
				arena.clearArena();
				arena.setUsed(false);
			}
		}
	}
	
	public static BloostryPractice getInstance() {
		return instance;
	}
}