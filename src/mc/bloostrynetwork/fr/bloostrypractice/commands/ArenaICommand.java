package mc.bloostrynetwork.fr.bloostrypractice.commands;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.IntegerUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.Arena;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaAbstract;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaRush;
import mc.bloostrynetwork.fr.bloostrypractice.utils.world.Cuboide;

public class ArenaICommand implements ICommand {

	public static ArrayList<String> playersInCreateMode = new ArrayList<>();

	public static HashMap<String, Location> rightClick = new HashMap<>();
	public static HashMap<String, Location> leftClick = new HashMap<>();

	public static HashMap<String, Location> spawnLoc1 = new HashMap<>();
	public static HashMap<String, Location> spawnLoc2 = new HashMap<>();

	public static HashMap<String, Location> bedLoc1 = new HashMap<>();
	public static HashMap<String, Location> bedLoc2 = new HashMap<>();

	@Override
	public String getCommand() {
		return "arena";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length == 0) {
			commandSender.sendMessage("�8�m" + PracticeManager.LINE_SPACER);
			commandSender.sendMessage("");
			commandSender.sendMessage("�eListe des commmandes:");
			commandSender.sendMessage("  �6- �f/arena createmode");
			commandSender.sendMessage("  �6- �f/arena create �e<ar�ne>");
			commandSender.sendMessage("  �6- �f/arena <spawnLoc1/spawnLoc2>");
			commandSender.sendMessage("  �6- �f/arena info �e<ar�ne> <id>");
			commandSender.sendMessage("  �6- �f/arena teleport �e<ar�ne> <id>");
			commandSender.sendMessage("");
			commandSender.sendMessage("�8�m" + PracticeManager.LINE_SPACER);
		} else {
			if (commandSender instanceof Player) {
				BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.RESP_DEVELOPER.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							PracticeManager.PREFIX + "�CVous n'avez pas le rang requis pour �x�cuter cette commande !");
					return;
				}
			}
			if (args.length >= 1) {
				String arg1 = args[0];
				if (arg1.equalsIgnoreCase("spawnLoc2")) {
					if (commandSender instanceof Player) {
						Player player = (Player) commandSender;
						Location generateLocation = player.getLocation();
						spawnLoc2.put(player.getName(), generateLocation);
						commandSender.sendMessage(PracticeManager.PREFIX + "�fD�finition du �e2�me�f point de spawn.");
					} else {
						commandSender.sendMessage(PracticeManager.PREFIX
								+ "�cErreur, vous devez �tre un joueur pour �x�cuter cette commande !");
					}
				} else if (arg1.equalsIgnoreCase("spawnLoc1")) {
					if (commandSender instanceof Player) {
						Player player = (Player) commandSender;
						Location generateLocation = player.getLocation();
						spawnLoc1.put(player.getName(), generateLocation);
						commandSender.sendMessage(PracticeManager.PREFIX + "�fD�finition du �e1er�f point de spawn.");
					} else {
						commandSender.sendMessage(PracticeManager.PREFIX
								+ "�cErreur, vous devez �tre un joueur pour �x�cuter cette commande !");
					}
				} else if (arg1.equalsIgnoreCase("createmode")) {
					if (commandSender instanceof Player) {
						Player player = (Player) commandSender;
						if (playersInCreateMode.contains(player.getName())) {
							playersInCreateMode.remove(player.getName());
							player.sendMessage(
									PracticeManager.PREFIX + "�fVous n'�tes plus en mode cr�ation d'ar�ne !");
						} else {
							playersInCreateMode.add(player.getName());
							player.sendMessage(PracticeManager.PREFIX + "�fVous �tes en mode cr�ation d'ar�ne !");
						}
					} else {
						commandSender.sendMessage(
								PracticeManager.PREFIX + "�cErreur, essayez: �f/arena autocreate �e<ar�ne>");
					}
				} else if (arg1.equalsIgnoreCase("autocreate")) {
					if (args.length == 2) {
						String name = args[1];

						if (name.equalsIgnoreCase("Rush")) {
							commandSender
									.sendMessage(PracticeManager.PREFIX + "�fD�marrage de la cr�ation de l'ar�ne !");
							ArenaRush.createArenaAuto(name);
							commandSender.sendMessage(PracticeManager.PREFIX + "�fCr�ation de l'ar�ne termin�e !");

							ArenaRush arena = ArenaRush.getLastArena(name);

							info(commandSender, arena);
						} else {
							commandSender
									.sendMessage(PracticeManager.PREFIX + "�fD�marrage de la cr�ation de l'ar�ne !");
							Arena.createArenaAuto(name);
							commandSender.sendMessage(PracticeManager.PREFIX + "�fCr�ation de l'ar�ne termin�e !");

							Arena arena = Arena.getLastArena(name);

							info(commandSender, arena);
						}
					} else {
						commandSender
								.sendMessage(PracticeManager.PREFIX + "�cErreur, essayez: �f/arena create �e<ar�ne>");
					}
				} else if (arg1.equalsIgnoreCase("create")) {
					if (commandSender instanceof Player) {
						Player player = (Player) commandSender;
						if (args.length == 2) {
							String name = args[1];
							/*
							 * int[] gameForThisMap = new int[] {};
							 * if (name.equalsIgnoreCase("Rush")) { gameForThisMap = new int[] {
							 * GameType.RUSH.getID() }; } else { gameForThisMap = new int[] {
							 * GameType.BUILDUHC.getID(), GameType.DEBUFF.getID(),
							 * GameType.NODEBUFF.getID(), GameType.GAPPLE.getID() }; }
							 */
							Location generateLocation = player.getLocation();
							Location location1 = spawnLoc1.get(player.getName());
							Location location2 = spawnLoc2.get(player.getName());

							if (rightClick.get(player.getName()) == null || leftClick.get(player.getName()) == null) {
								commandSender
										.sendMessage(PracticeManager.PREFIX + "�cVeuillez s�lectionner toutes l'ar�ne");
								return;
							}

							if (spawnLoc1.get(player.getName()) == null || spawnLoc2.get(player.getName()) == null) {
								commandSender
										.sendMessage(PracticeManager.PREFIX + "�cVeuillez s�lectionner d�finir les point d'apparition des joueurs");
								return;
							}
							
							Cuboide cuboide = new Cuboide(rightClick.get(player.getName()),
									leftClick.get(player.getName()));

							if (name.equalsIgnoreCase("Rush")) {
								if (bedLoc1.get(player.getName()) == null || bedLoc2.get(player.getName()) == null) {
									commandSender
											.sendMessage(PracticeManager.PREFIX + "�cVeuillez s�lectionner les lit des joueur");
									return;
								}
								
								Location bedLocation1 = bedLoc1.get(player.getName());
								Location bedLocation2 = bedLoc2.get(player.getName());
								
								commandSender.sendMessage(
										PracticeManager.PREFIX + "�fD�marrage de la cr�ation de l'ar�ne !");
								ArenaRush.createArena(name, generateLocation, location1, location2, cuboide, bedLocation1, bedLocation2);
								commandSender.sendMessage(PracticeManager.PREFIX + "�fCr�ation de l'ar�ne termin�e !");

								ArenaRush arena = ArenaRush.getLastArena(name);

								info(commandSender, arena);
							} else {
								commandSender.sendMessage(
										PracticeManager.PREFIX + "�fD�marrage de la cr�ation de l'ar�ne !");
								Arena.createArena(name, generateLocation, location1, location2, cuboide);
								commandSender.sendMessage(PracticeManager.PREFIX + "�fCr�ation de l'ar�ne termin�e !");

								Arena arena = Arena.getLastArena(name);

								info(commandSender, arena);
							}
						} else {
							commandSender.sendMessage(
									PracticeManager.PREFIX + "�cErreur, essayez: �f/arena create �e<ar�ne>");
						}
					} else {
						commandSender.sendMessage(PracticeManager.PREFIX
								+ "�cErreur, vous devez �tre un joueur pour �x�cuter cette commande !");
					}
				} else if (arg1.equalsIgnoreCase("info")) {
					if (args.length == 3) {
						String arg2 = args[1];
						String arg3 = args[2];

						if (IntegerUtils.isInteger(arg3)) {
							Integer id = IntegerUtils.getValueOf(arg3);
							String name = arg2;
							
							if (name.equalsIgnoreCase("Rush")) {
								ArenaRush arena = ArenaRush.getLastArena(name);
								info(commandSender, arena);
							} else {
								Arena arena = Arena.getLastArena(name);
								info(commandSender, arena);
							}

						} else {
							commandSender.sendMessage(
									PracticeManager.PREFIX + " �cVeuillez d�finir un chiffre ou un nombre !");
						}
					} else {
						commandSender.sendMessage(PracticeManager.PREFIX + "�cErreur, essayez: �f/arena <nom> <id>");
					}
				} else if (arg1.equalsIgnoreCase("teleport")) {
					if (args.length == 3) {
						String arg2 = args[1];
						String arg3 = args[2];

						if (IntegerUtils.isInteger(arg3)) {
							Integer id = IntegerUtils.getValueOf(arg3);
							String name = arg2;
							Arena arena = Arena.getArena(name, id);
							if (arena != null) {
								if (commandSender instanceof Player) {
									((Player) commandSender).teleport(arena.getFirstLocation());
									commandSender.sendMessage(PracticeManager.PREFIX + "�fT�l�portation vers l'ar�ne �e"
											+ arena.getName() + " �f#�e" + arena.getId());
								} else {
									commandSender.sendMessage(PracticeManager.PREFIX + "�cVous n'�tes pas un joueur");
								}
							} else {
								commandSender.sendMessage(PracticeManager.PREFIX + "�cCette ar�ne n'existe pas !");
							}
						} else {
							commandSender.sendMessage(
									PracticeManager.PREFIX + " �cVeuillez d�finir un chiffre ou un nombre !");
						}
					} else {
						commandSender.sendMessage(PracticeManager.PREFIX + "�cErreur, essayez: �f/arena <nom> <id>");
					}
				} else {
					commandSender.sendMessage(PracticeManager.PREFIX + "�cErreur, essayez: �f/arena");
				}
			}
		}
	}

	private void info(CommandSender commandSender, ArenaAbstract arena) {
		if (arena != null) {

			Location cuboideLocation1 = arena.getCuboide().getLocation1();
			Location cuboideLocation2 = arena.getCuboide().getLocation2();

			Location playerFightLocation1 = arena.getFirstLocation();
			Location playerFightLocation2 = arena.getSecondLocation();

			Location generationLocation = arena.getGenerationLocation();

			commandSender.sendMessage("�8�m" + PracticeManager.LINE_SPACER);
			commandSender.sendMessage("");
			commandSender.sendMessage("�fInformation sur l'ar�ne �e" + arena.getName() + " �f#�e" + arena.getId());
			commandSender.sendMessage("  �fCuboide:");
			commandSender.sendMessage("    �flocation 1: �e" + cuboideLocation1.getBlockX() + "�f, "
					+ cuboideLocation1.getBlockY() + "�f, �e" + cuboideLocation1.getBlockZ());
			commandSender.sendMessage("    �flocation 2: �e" + cuboideLocation2.getBlockX() + "�f, "
					+ cuboideLocation2.getBlockY() + "�f, �e" + cuboideLocation2.getBlockZ());
			commandSender.sendMessage("  �f");
			commandSender.sendMessage("  �fPosition des joueur au combat:");
			commandSender.sendMessage("    �flocation 1: �e" + playerFightLocation1.getBlockX() + "�f, "
					+ playerFightLocation1.getBlockY() + "�f, �e" + playerFightLocation1.getBlockZ());
			commandSender.sendMessage("    �flocation 2: �e" + playerFightLocation2.getBlockX() + "�f, "
					+ playerFightLocation2.getBlockY() + "�f, �e" + playerFightLocation2.getBlockZ());
			commandSender.sendMessage("  �f");
			if (arena.getName().equalsIgnoreCase("Rush")) {

				Location bedLocation1 = ((ArenaRush) arena).getBedLocation1();
				Location bedLocation2 = ((ArenaRush) arena).getBedLocation2();

				commandSender.sendMessage("  �fPosition des lit des joueurs:");
				commandSender.sendMessage("    �flocation 1: �e" + bedLocation1.getBlockX() + "�f, "
						+ bedLocation1.getBlockY() + "�f, �e" + bedLocation1.getBlockZ());
				commandSender.sendMessage("    �flocation 2: �e" + bedLocation2.getBlockX() + "�f, "
						+ bedLocation2.getBlockY() + "�f, �e" + bedLocation2.getBlockZ());
				commandSender.sendMessage("  �f");
			}
			commandSender.sendMessage("  �fPosition de la g�n�ration de la schematics:");
			commandSender.sendMessage("    �flocation 1: �e" + generationLocation.getBlockX() + "�f, "
					+ generationLocation.getBlockY() + "�f, �e" + generationLocation.getBlockZ());
			commandSender.sendMessage("  �f");
			commandSender.sendMessage("  �fUtilisation: " + (arena.isUsed() ? "�aOui" : "�cNon"));
			commandSender.sendMessage("");
			Fight fight = arena.getFight();
			if (arena.isUsed() && fight != null) {
				commandSender.sendMessage("  �fMatch information:");
				commandSender.sendMessage("    �fJoueurs:");
				for (String players : fight.players) {
					commandSender.sendMessage("    �7- �a" + players);
				}
				commandSender.sendMessage("    �fAdversaire:");
				for (String players : fight.oppenents) {
					commandSender.sendMessage("    �7- �c" + players);
				}
				commandSender.sendMessage("    �fDur�e: �e" + DateUtils.getTimerDate(fight.getTimeElapsed()));
				commandSender.sendMessage("    �FMode de jeu: �e" + fight.getGameType().getName());
				commandSender.sendMessage("    �FType de combat: �e" + fight.getFightType().getName());
			}
			commandSender.sendMessage("");
			commandSender.sendMessage("�8�m" + PracticeManager.LINE_SPACER);
		} else {
			commandSender.sendMessage(PracticeManager.PREFIX + "�cCette ar�ne n'existe pas !");
		}
	}
}
