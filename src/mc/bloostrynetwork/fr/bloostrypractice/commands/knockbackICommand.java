package mc.bloostrynetwork.fr.bloostrypractice.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;

public class knockbackICommand implements ICommand {

	@Override
	public String getCommand() {
		return "knockback";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length == 0) {
			commandSender.sendMessage("�8�M" + PracticeManager.LINE_SPACER);
			commandSender.sendMessage("");
			commandSender.sendMessage("�6Configuration du recul des joueur");
			commandSender.sendMessage("  �e- �f/knockback <gameType> <sprint/no-sprint> setver <decimal/entier>");
			commandSender.sendMessage("  �e- �f/knockback <gameType> <sprint/no-sprint> sethor <decimal/entier>");
			commandSender.sendMessage("");
			commandSender.sendMessage("�8�M" + PracticeManager.LINE_SPACER);
		} else if (args.length == 4) {
			if (commandSender instanceof Player) {
				BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.RESP_DEVELOPER.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							PracticeManager.PREFIX + "�CVous n'avez pas le rang requis pour �x�cuter cette commande !");
					return;
				}
			}

			String gameType = args[0];

			if (args[1].equalsIgnoreCase("sprint")) {
				if (args[2].equalsIgnoreCase("setver")) {
					if (isDouble(args[3])) {
						Double value = getDouble(args[3]);
						commandSender.sendMessage(PracticeManager.PREFIX + "�6D�finition du nouveau recul en hauteur �e"
								+ value + "�6 pour le �e" + gameType + "�6.");
						BloostryPractice.getInstance().getConfig().set("knockback-configuration."+gameType+".sprint.ver", value);
						BloostryPractice.getInstance().saveConfig();
					} else {
						commandSender.sendMessage(
								PracticeManager.PREFIX + "�cErreur, veuillez d�finir un entier ou un d�cimal !");
					}
				} else if (args[2].equalsIgnoreCase("sethor")) {
					if (isDouble(args[3])) {
						Double value = getDouble(args[3]);
						commandSender.sendMessage(PracticeManager.PREFIX + "�6D�finition du nouveau recul en arri�re �e"
								+ value + "�6 pour le �e" + gameType + "�6.");
						BloostryPractice.getInstance().getConfig().set("knockback-configuration."+gameType+".sprint.hor", value);
						BloostryPractice.getInstance().saveConfig();
					} else {
						commandSender.sendMessage(
								PracticeManager.PREFIX + "�cErreur, veuillez d�finir un entier ou un d�cimal !");
					}
				} else {
					commandSender.sendMessage(PracticeManager.PREFIX + "�CErreur, essayez: �e/knockback");
				}
			} else if (args[0].equalsIgnoreCase("no-sprint")) {
				if (args[1].equalsIgnoreCase("setver")) {
					if (isDouble(args[2])) {
						Double value = getDouble(args[2]);
						commandSender.sendMessage(PracticeManager.PREFIX + "�6D�finition du nouveau recul en hauteur �e"
								+ value + "�6 pour le �e" + gameType + "�6.");
						BloostryPractice.getInstance().getConfig().set("knockback-configuration."+gameType+".no-sprint.ver", value);
						BloostryPractice.getInstance().saveConfig();
					} else {
						commandSender.sendMessage(
								PracticeManager.PREFIX + "�cErreur, veuillez d�finir un entier ou un d�cimal !");
					}
				} else if (args[1].equalsIgnoreCase("sethor")) {
					if (isDouble(args[2])) {
						Double value = getDouble(args[2]);
						commandSender.sendMessage(PracticeManager.PREFIX + "�6D�finition du nouveau recul en arri�re �e"
								+ value + "�6 pour le �e" + gameType + "�6.");
						BloostryPractice.getInstance().getConfig().set("knockback-configuration."+gameType+".no-sprint.hor", value);
						BloostryPractice.getInstance().saveConfig();
					} else {
						commandSender.sendMessage(
								PracticeManager.PREFIX + "�cErreur, veuillez d�finir un entier ou un d�cimal !");
					}
				} else {
					commandSender.sendMessage(PracticeManager.PREFIX + "�CErreur, essayez: �e/knockback");
				}
			} else {

			}
		} else {
			commandSender.sendMessage(PracticeManager.PREFIX + "�CErreur, essayez: �e/knockback");
		}
	}

	private boolean isDouble(String value) {
		return getDouble(value) != null;
	}

	private Double getDouble(String value) {
		try {
			return Double.valueOf(value);
		} catch (Exception e) {
			return null;
		}
	}
}
