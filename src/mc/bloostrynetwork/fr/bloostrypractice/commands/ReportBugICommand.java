package mc.bloostrynetwork.fr.bloostrypractice.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;

public class ReportBugICommand implements ICommand {

	@Override
	public String getCommand() {
		return "reportbug";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length == 0) {
			commandSender.sendMessage(PracticeManager.PREFIX + "�cErreur, essayez: �e/reportbug <message>");
			return;
		}

		if (args.length == 1) {
			if (commandSender instanceof Player) {
				BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.RESP_DEVELOPER.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							PracticeManager.PREFIX + "�CVous n'avez pas le rang requis pour �x�cuter cette commande !");
					return;
				}
			}
			if (args[0].equalsIgnoreCase("list")) {
				File reportFile = new File(BloostryPractice.FOLDER_CORE.getAbsolutePath() + "/reportbug.yml");
				if (!reportFile.exists()) {
					commandSender.sendMessage("�cAucun signalement � �t� post� !");
					return;
				}

				FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(reportFile);
			
				HashMap<String, List<String>> reports = new HashMap<>();
				
				for(String playerNameReports : fileConfiguration.getKeys(false)) {
					@SuppressWarnings("unchecked")
					List<String> reportList = (List<String>) fileConfiguration.getList(playerNameReports);
					if (reportList != null) {
						reports.put(playerNameReports, reportList);
					}
				}
				
				if(reports.isEmpty()) {
					commandSender.sendMessage(PracticeManager.PREFIX+"�cAucun signalement � �t� post� !");
				} else {
					commandSender.sendMessage("�8�m"+PracticeManager.LINE_SPACER);
					commandSender.sendMessage("");
					commandSender.sendMessage("�FListe des report:");
					for(String author : reports.keySet()) {
						commandSender.sendMessage("  �e"+author);
						List<String> listMessage = reports.get(author);
						commandSender.sendMessage("  �6- �f"+listMessage);
					}
					commandSender.sendMessage("");
					commandSender.sendMessage("�8�m"+PracticeManager.LINE_SPACER);
				}
				return;
			} else if (args[0].equalsIgnoreCase("clear")) {
				File reportFile = new File(BloostryPractice.FOLDER_CORE.getAbsolutePath() + "/reportbug.yml");
				if (reportFile.exists()) {
					reportFile.delete();
					commandSender.sendMessage(PracticeManager.PREFIX + "�cN�ttoyage de la liste des bugs");
				}
				return;
			}
		}

		String message = "";

		for (int i = 0; i < args.length; i++) {
			if (i == 0)
				message = args[i];
			else
				message += " " + args[i];
		}

		try {
			File reportFile = new File(BloostryPractice.FOLDER_CORE.getAbsolutePath() + "/reportbug.yml");
			if (!reportFile.exists())
				try {
					reportFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}

			FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(reportFile);

			@SuppressWarnings("unchecked")
			List<String> reportList = (List<String>) fileConfiguration.getList(commandSender.getName() + "");
			if (reportList != null) {
			} else {
				reportList = new ArrayList<>();
			}

			reportList.add(message);

			fileConfiguration.set(commandSender.getName(), reportList);
			fileConfiguration.save(reportFile);

			commandSender.sendMessage(PracticeManager.PREFIX
					+ "�aVotre signalement � bien �t� envoy�, Merci d'avoir aider le d�veloppement du serveur.");
		} catch (Exception e) {
			e.printStackTrace();
			commandSender.sendMessage(
					PracticeManager.PREFIX + "�cNous sommes d�soler mais votre signalement n'a pu �tre envoy� !");
		}
	}
}
