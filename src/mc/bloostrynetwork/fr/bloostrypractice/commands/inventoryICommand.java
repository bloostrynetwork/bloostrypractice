package mc.bloostrynetwork.fr.bloostrypractice.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.inventory.InventoryDuel;

public class inventoryICommand implements ICommand {

	@Override
	public String getCommand() {
		return "inventory";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender sender, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			if (args.length == 1) {
				Player target = Bukkit.getPlayer(args[0]);
				if (target != null) {
					if (InventoryDuel.inventorys.containsKey(target)) {
						((InventoryDuel) InventoryDuel.inventorys.get(target)).openInventory(player);
					} else {
						player.sendMessage(PracticeManager.PREFIX + "�cCe joueur n'a pas de inventaire de combat !");
					}
				} else {
					player.sendMessage(PracticeManager.PREFIX + "�cCe joueur n'est pas en ligne !");
				}
			} else {
				player.sendMessage(PracticeManager.PREFIX + "�cErreur, essayez: �e/inv �f<joueur>");
			}
		}
	}
}
