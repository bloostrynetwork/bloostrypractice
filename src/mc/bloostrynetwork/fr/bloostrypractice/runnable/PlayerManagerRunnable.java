package mc.bloostrynetwork.fr.bloostrypractice.runnable;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PlayerManagerRunnable implements Runnable {

	private BukkitTask bukkitTask;
	
	public PlayerManagerRunnable() {
		bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(BloostryPractice.getInstance(), this, 0, 10);
	}

	@Override
	public void run() {
		for(PracticePlayer practicePlayers : PracticeManager.getInstance().practicePlayers.values()) {
			practicePlayers.getPlayerScoreboard().refresh();
		}
	}
	
	public BukkitTask getBukkitTask() {
		return this.bukkitTask;
	}
}
