package mc.bloostrynetwork.fr.bloostrypractice.manager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.BlockListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.EnderPearlsPatch;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.EntityDamageListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.EntityExplodeListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.EntityRegainHealthListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.InventoryClickListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerConnectionListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerDeathListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerFoodLevelChangeListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerInteractListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerItemConsumeListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerItemDropListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerMoveListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerTeleportListener;
import mc.bloostrynetwork.fr.bloostrypractice.listeners.PlayerVelocityListener;

public class EventsManager {

	public void registerListeners() {
		JavaPlugin javaPlugin = BloostryPractice.getInstance();
		PluginManager pluginManager = Bukkit.getPluginManager();
		
		pluginManager.registerEvents(new PlayerConnectionListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerItemDropListener(), javaPlugin);
		pluginManager.registerEvents(new EntityDamageListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerFoodLevelChangeListener(), javaPlugin);
		pluginManager.registerEvents(new BlockListener(), javaPlugin);
		pluginManager.registerEvents(new InventoryClickListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerInteractListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerDeathListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerVelocityListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerMoveListener(), javaPlugin);
		pluginManager.registerEvents(new PlayerItemConsumeListener(), javaPlugin);
		pluginManager.registerEvents(new EntityRegainHealthListener(), javaPlugin);
		pluginManager.registerEvents(new EnderPearlsPatch(), javaPlugin);
		pluginManager.registerEvents(new PlayerTeleportListener(), javaPlugin);
		pluginManager.registerEvents(new EntityExplodeListener(), javaPlugin);
		
		EnderPearlsPatch.repeatTimer();
	}
}
