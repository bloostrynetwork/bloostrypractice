package mc.bloostrynetwork.fr.bloostrypractice.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.fr.bloostrypractice.BloostryPractice;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.Fight;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaAbstract;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.Arena;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.arena.ArenaRush;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.FightType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.queue.Queue;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PlayerStatus;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class PracticeManager {

	public static String PREFIX = "�8[�cPractice�8] ";
	public static String LINE_SPACER = "-----------------------------------------------------";

	private static PracticeManager instance;

	public HashMap<String, PracticePlayer> practicePlayers = new HashMap<>();

	public HashMap<GameType, Queue> unrankedQueue = new HashMap<>();
	public HashMap<GameType, Queue> rankedQueue = new HashMap<>();
	public HashMap<GameType, Queue> teamFightQueue = new HashMap<>();

	public List<String> arenasName = new ArrayList<>();
	public HashMap<String, ArrayList<ArenaAbstract>> arenas = new HashMap<>();

	public PracticeManager() {
		instance = this;

		for (GameType gameType : GameType.values()) {
			unrankedQueue.put(gameType, new Queue(FightType.UNRANKED, gameType));
			rankedQueue.put(gameType, new Queue(FightType.RANKED, gameType));
			teamFightQueue.put(gameType, new Queue(FightType.TEAM_FIGHT, gameType));
		}
	}

	public void initArena() {
		for (File folders : BloostryPractice.ARENA_FOLDER.listFiles()) {
			if (folders.isDirectory()) {
				for (File arenaFiles : folders.listFiles()) {
					Console.sendMessageInfo("�bRecovered file " + arenaFiles.getName());
					String name = arenaFiles.getName().replace(".json", "").split("-")[0];
					int id = Integer.valueOf(arenaFiles.getName().replace(".json", "").split("-")[1]);
					ArenaAbstract arena = Arena.getArena(name, id);
					if (arena == null) {
						Console.sendMessageError("�cAdded failed arena �F" + name + " �7[�6ID: �e" + id + "�7]");
					} else {
						if(arena.getName().equalsIgnoreCase("Rush")) {
							arena = ArenaRush.getArena(name, id); 
							arenasName.add(name);
							ArrayList<ArenaAbstract> arenaList = arenas.get(arena.getName());
							if (arenaList == null)
								arenaList = new ArrayList<>();
							arenaList.add(arena);
							arena.setUsed(false);
							arenas.put(arena.getName(), arenaList);
						} else {
							arenasName.add(name);
							ArrayList<ArenaAbstract> arenaList = arenas.get(arena.getName());
							if (arenaList == null)
								arenaList = new ArrayList<>();
							arenaList.add(arena);
							arena.setUsed(false);
							arenas.put(arena.getName(), arenaList);	
						}
						Console.sendMessageInfo("�aAdded arena �F" + arena.getName() + " �7[�6ID: �e" + id + "�7]");
					}
				}
			}
		}
	}

	public void joinQueue(FightType fightType, GameType gameType, PracticePlayer practicePlayer) {
		Player player = practicePlayer.getPlayer();
		player.closeInventory();
		player.sendMessage(PREFIX + "�fVous avez rejoint la file d'attente �e" + gameType.getName() + " �fen �e"
				+ fightType.getName() + "�f.");
		Queue queue = getQueue(fightType, gameType);
		if (queue.getPlayersInQueue().size() >= 1) {
			PracticePlayer practicePlayerOppenent = queue.getPlayersInQueue()
					.get(new Random().nextInt(queue.getPlayersInQueue().size()));
			Player oppenent = practicePlayerOppenent.getPlayer();
			player.sendMessage(PREFIX + "�fVous avez trouver une joueur �e" + oppenent.getName());
			oppenent.sendMessage(PREFIX + "�fVous avez trouver une joueur �e" + player.getName());
			player.sendMessage(PREFIX + "�fRecherche d'une map disponible");
			oppenent.sendMessage(PREFIX + "�fRecherche d'une map disponible");
		
			if(gameType == GameType.RUSH) {
				ArrayList<ArenaAbstract> arena = arenas.get("Rush");

				ArenaAbstract gameArena = null;

				for (ArenaAbstract arenas : arena) {
					if (!(arenas.isUsed())) {
						gameArena = arenas;
						break;
					}
				}

				if (gameArena == null) {
					player.sendMessage(PREFIX + "�CAucune ar�ne n'est disponible !");
					oppenent.sendMessage(PREFIX + "�CAucune ar�ne n'est disponible !");

					leaveQueue(practicePlayerOppenent);
					leaveQueue(practicePlayer);

					practicePlayerOppenent.clearStuff();
					practicePlayerOppenent.joinInSpawn();

					practicePlayer.clearStuff();
					practicePlayer.joinInSpawn();
				} else {
					practicePlayerOppenent.clearStuff();
					practicePlayer.clearStuff();

					leaveQueue(practicePlayerOppenent);
					leaveQueue(practicePlayer);

					player.sendMessage(PREFIX + "�fLancement de la partie en cours sur la map �e" + gameArena.getName());
					oppenent.sendMessage(PREFIX + "�fLancement de la partie en cours sur la map �e" + gameArena.getName());
					Fight fight = new Fight(player, oppenent, gameArena, gameType, fightType);
					fight.start();
				}
			} else {
				String arenaName = arenasName.get(new Random().nextInt(arenas.size()));
				while(arenaName.equalsIgnoreCase("rush")) {
					arenaName = arenasName.get(new Random().nextInt(arenas.size()));
				}
				ArrayList<ArenaAbstract> arena = arenas.get(arenaName);

				ArenaAbstract gameArena = null;

				for (ArenaAbstract arenas : arena) {
					if (!(arenas.isUsed())) {
						gameArena = arenas;
						break;
					}
				}

				if (gameArena == null) {
					for (String arenaNameVar : arenas.keySet()) {
						for (ArenaAbstract arenas : arenas.get(arenaNameVar)) {
							if (!(arenas.isUsed())) {
								gameArena = arenas;
								break;
							}
						}
					}
				}

				if (gameArena == null) {
					player.sendMessage(PREFIX + "�CAucune ar�ne n'est disponible !");
					oppenent.sendMessage(PREFIX + "�CAucune ar�ne n'est disponible !");

					leaveQueue(practicePlayerOppenent);
					leaveQueue(practicePlayer);

					practicePlayerOppenent.clearStuff();
					practicePlayerOppenent.joinInSpawn();

					practicePlayer.clearStuff();
					practicePlayer.joinInSpawn();
				} else {
					practicePlayerOppenent.clearStuff();
					practicePlayer.clearStuff();

					leaveQueue(practicePlayerOppenent);
					leaveQueue(practicePlayer);

					player.sendMessage(PREFIX + "�fLancement de la partie en cours sur la map �e" + gameArena.getName());
					oppenent.sendMessage(PREFIX + "�fLancement de la partie en cours sur la map �e" + gameArena.getName());
					Fight fight = new Fight(player, oppenent, gameArena, gameType, fightType);
					fight.start();
				}	
			}
		} else {
			player.sendMessage(PREFIX + "�fVeuillez patientez qu'un joueur rejoint la file d'attente ...");
			queue.getPlayersInQueue().add(practicePlayer);
			practicePlayer.clearStuff();
			practicePlayer.joinInQueue();
		}
	}

	public void informationQueue(PracticePlayer practicePlayer) {
		Queue queue = getQueue(practicePlayer);
		Player player = practicePlayer.getPlayer();
		if (queue == null) {
			player.sendMessage(PREFIX + "�cVous n'�tes dans aucune file d'attente !");
			return;
		}
		player.sendMessage("�8�m" + LINE_SPACER);
		player.sendMessage("");
		player.sendMessage("�fVous avez rejoint la file d'attente �e" + queue.getGameType().getName() + " �fen �e"
				+ queue.getFightType().getName() + "�f.");
		player.sendMessage("");
		player.sendMessage("�8�m" + LINE_SPACER);
	}

	public void leaveQueue(PracticePlayer practicePlayer) {
		Queue queue = getQueue(practicePlayer);
		if (queue == null) {
			return;
		}
		queue.getPlayersInQueue().remove(practicePlayer);
	}

	public Queue getQueue(FightType fightType, GameType gameType) {
		if (fightType == FightType.UNRANKED) {
			return unrankedQueue.get(gameType);
		} else if (fightType == FightType.RANKED) {
			return rankedQueue.get(gameType);
		} else if (fightType == FightType.TEAM_FIGHT) {
			return teamFightQueue.get(gameType);
		} else
			return null;
	}

	public void addPracticePlayer(Player player) {
		if (!practicePlayers.containsKey(player.getName())) {
			practicePlayers.put(player.getName(), new PracticePlayer(player.getName()));
			Console.sendMessageDebug("addPlayerPlayer(" + player.getName() + ")");
		}
	}

	public void removePracticePlayer(Player player) {
		if (practicePlayers.containsKey(player.getName())) {
			practicePlayers.remove(player.getName());
			Console.sendMessageDebug("removePlayerPlayer(" + player.getName() + ")");
		}
	}

	public boolean isInQueue(PracticePlayer practicePlayer) {
		if (getQueue(practicePlayer) == null)
			return false;
		return true;
	}

	public Queue getQueue(PracticePlayer practicePlayer) {
		for (FightType fightType : FightType.values()) {
			if (fightType.hasQueue()) {
				for (GameType gameType : GameType.values()) {
					Queue queue = getQueue(fightType, gameType);
					ArrayList<PracticePlayer> playersListQueue = queue.getPlayersInQueue();
					ArrayList<PracticePlayer> playersListFight = queue.getPlayersInFight();
					if (playersListQueue.contains(practicePlayer) || playersListFight.contains(practicePlayer)) {
						return queue;
					}
				}
			}
		}
		return null;
	}

	public static PracticeManager getInstance() {
		return instance;
	}

	public PracticePlayer getPracticePlayer(String name) {
		return practicePlayers.get(name);
	}

	public Location getSpawnLocation() {
		return new Location(Bukkit.getWorld("world"), -737, 58, -1254, 90, 0);
	}
	
	public Location getEditKitSpawnLocation() {
		return new Location(Bukkit.getWorld("world"), -720, 61, -1215);
	}
	
	private HashMap<String, GameType> kitEditor = new HashMap<>();

	public void joinEditKit(GameType gameType, PracticePlayer practicePlayer) {
		Player player = practicePlayer.getPlayer();
		if(player == null)return;
		player.teleport(getEditKitSpawnLocation());
		gameType.getIKit().giveKitDefault(player);
		practicePlayer.setPlayerStatus(PlayerStatus.INEDITKIT);
		kitEditor.put(player.getName(), gameType);
		player.sendMessage(PREFIX + "�fT�l�portation pour la modification du kit �e"+gameType.getName()+"�f.");
	}
	
	public void saveKit(GameType gameType, PracticePlayer practicePlayer) {
		Player player = practicePlayer.getPlayer();
		gameType.getIKit().save(player);
		practicePlayer.joinInSpawn();
		practicePlayer.setPlayerStatus(PlayerStatus.INSPAWN);
		practicePlayer.teleportToSpawn();
		player.sendMessage(PREFIX + "�fVous venez d'enregistrer votre kit �e"+gameType.getName()+"�f.");
		kitEditor.remove(player.getName());
	}
	
	public void saveKit(PracticePlayer practicePlayer) {
		Player player = practicePlayer.getPlayer();
		if(player == null)return;
		GameType gameType = kitEditor.get(player.getName());
		if(gameType == null)return;
		saveKit(gameType, practicePlayer);
	}
	
	public void leaveEditKit(GameType gameType, PracticePlayer practicePlayer) {
		Player player = practicePlayer.getPlayer();
		if(player == null)return;
		kitEditor.remove(player.getName());
		practicePlayer.joinInSpawn();
		practicePlayer.setPlayerStatus(PlayerStatus.INSPAWN);
		practicePlayer.teleportToSpawn();
		player.sendMessage(PREFIX + "�CVous avez quitter la modification de kit.");
	}

	public void leaveEditKit(PracticePlayer practicePlayer) {
		Player player = practicePlayer.getPlayer();
		if(player == null)return;
		GameType gameType = kitEditor.get(player.getName());
		if(gameType == null)return;
		leaveEditKit(gameType, practicePlayer);
	}
}
