package mc.bloostrynetwork.fr.bloostrypractice.gui.spawn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.IGui;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.Inventory;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.ItemHandler;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class GuiEditKit implements IGui {

	public static final Inventory GUI = new Inventory(null, "�9Modifier de kit", 9);

	public static final HashMap<Integer, Item> items = new HashMap<>();

	public void refresh(Player player) {
		int i = 1;
		for (GameType gameType : GameType.values()) {
			List<String> lore = new ArrayList<>();
			lore.add("�8�m------------------------------------");
			lore.add("");
			lore.add("�fCliquer pour aller modifier le kit en:");
			lore.add("�e"+gameType.getName());
			lore.add("");
			lore.add("�8�m------------------------------------");
			items.put(i, new Item(gameType.getIcon().getType(), gameType.getData(),
					gameType.getColoredName(), lore, new ItemHandler() {
						@Override
						public void handle(Player player) {
							PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
							PracticeManager.getInstance().joinEditKit(gameType, practicePlayer);
						}
					}));
			i++;
		}

		org.bukkit.inventory.Inventory invOpen = GUI.toInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}

	public Inventory getGUI() {
		return GUI;
	}

	@Override
	public void open(Player player) {
		player.closeInventory();
		int i = 1;
		for (GameType gameType : GameType.values()) {
			List<String> lore = new ArrayList<>();
			lore.add("�8�m------------------------------------");
			lore.add("");
			lore.add("�fCliquer pour aller modifier le kit en:");
			lore.add("�e"+gameType.getName());
			lore.add("");
			lore.add("�8�m------------------------------------");
			items.put(i, new Item(gameType.getIcon().getType(), gameType.getData(),
					gameType.getColoredName(), lore, new ItemHandler() {
						@Override
						public void handle(Player player) {
							PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
							PracticeManager.getInstance().joinEditKit(gameType, practicePlayer);
						}
					}));
			i++;
		}

		org.bukkit.inventory.Inventory invOpen = GUI.toInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
		player.openInventory(GUI.toInventory());
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);
				}
			}
		}
	}
}
