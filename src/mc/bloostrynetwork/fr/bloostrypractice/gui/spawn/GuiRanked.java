package mc.bloostrynetwork.fr.bloostrypractice.gui.spawn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.GameType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.game.fight.FightType;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.IGui;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.Inventory;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.ItemHandler;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class GuiRanked implements IGui {

	public static final Inventory GUI = new Inventory(null, "�9Ranked �8(�7class�8)", 9);

	public static final HashMap<Integer, Item> items = new HashMap<>();

	public void refresh(Player player) {
		int i = 1;
		for (GameType gameType : GameType.values()) {
			List<String> lore = new ArrayList<>();
			lore.add("�7�m------------------------------");
			lore.add("");
			lore.add("�cJoueur en attente: �f"
					+ PracticeManager.getInstance().getQueue(FightType.RANKED, gameType).getPlayersInQueue().size());
			lore.add("�cJoueur en combat: �f"
					+ PracticeManager.getInstance().getQueue(FightType.RANKED, gameType).getPlayersInFight().size());
			lore.add("");
			lore.add("�7�m------------------------------");
			items.put(i, new Item(gameType.getIcon().getType(), gameType.getData(),
					gameType.getColoredName(), lore, new ItemHandler() {
						@Override
						public void handle(Player player) {
							PracticePlayer practicePlayer = PracticeManager.getInstance().getPracticePlayer(player.getName());
							PracticeManager.getInstance().joinQueue(FightType.RANKED, gameType, practicePlayer);
						}
					}));
			i++;
		}

		org.bukkit.inventory.InventoryView invOpen = player.getOpenInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}

	public Inventory getGUI() {
		return GUI;
	}

	@Override
	public void open(Player player) {
		player.closeInventory();
		player.openInventory(GUI.toInventory());
		if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
			refresh(player);
		}
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);
				}
			}
		}
	}
}
