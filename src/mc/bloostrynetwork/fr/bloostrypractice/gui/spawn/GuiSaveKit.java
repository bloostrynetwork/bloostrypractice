package mc.bloostrynetwork.fr.bloostrypractice.gui.spawn;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.fr.bloostrypractice.manager.PracticeManager;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.IGui;
import mc.bloostrynetwork.fr.bloostrypractice.utils.gui.Inventory;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.Item;
import mc.bloostrynetwork.fr.bloostrypractice.utils.items.ItemHandler;
import mc.bloostrynetwork.fr.bloostrypractice.utils.player.PracticePlayer;

public class GuiSaveKit implements IGui {

	public static final Inventory GUI = new Inventory(null, "§9Sauvegarde de votre kit", 9);

	public static final HashMap<Integer, Item> items = new HashMap<>();

	public void refresh(Player player) {
		ItemStack deco = new ItemStack(Material.STAINED_GLASS_PANE);
		org.bukkit.inventory.Inventory inventory = GUI.toInventory();
		inventory.setItem(0, deco);
		inventory.setItem(1, deco);
		inventory.setItem(2, deco);
		inventory.setItem(4, deco);
		inventory.setItem(6, deco);
		inventory.setItem(7, deco);
		inventory.setItem(8, deco);

		items.put(4, new Item(Material.STAINED_GLASS_PANE, (byte) 14, "§cAnnuler la modification du kit", null,
				new ItemHandler() {
					@Override
					public void handle(Player player) {
						PracticePlayer practicePlayer = PracticeManager.getInstance()
								.getPracticePlayer(player.getName());
						PracticeManager.getInstance().leaveEditKit(practicePlayer);
					}
				}));

		items.put(6, new Item(Material.STAINED_GLASS_PANE, (byte) 5, "§aSauvegarde et quitter", null,
				new ItemHandler() {
					@Override
					public void handle(Player player) {
						PracticePlayer practicePlayer = PracticeManager.getInstance()
								.getPracticePlayer(player.getName());
						PracticeManager.getInstance().saveKit(practicePlayer);
					}
				}));

		org.bukkit.inventory.InventoryView invOpen = player.getOpenInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}

	public Inventory getGUI() {
		return GUI;
	}

	@Override
	public void open(Player player) {
		player.closeInventory();
		player.openInventory(GUI.toInventory());
		if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
			refresh(player);
		}
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);
				}
			}
		}
	}
}
